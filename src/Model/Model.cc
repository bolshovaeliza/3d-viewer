#include "Model.h"

int s21::Model::parseCoord(const char *cur) {
  size_t shift = 0;

  Reference_.vertices().push_back(std::stod(cur, &shift));
  return shift;
};

void s21::Model::addVertex(std::string VertexLine) {
  int countCoord = 0;
  const char *ptr = VertexLine.c_str();
  while (*ptr) {
    if (*ptr < 33 || *ptr == 'v') {
      ++ptr;
    } else if (isdigit(*ptr) || *ptr == '-') {
      ptr += s21::Model::parseCoord(ptr);
      ++countCoord;
    } else {
      Reference_ = initDefaultModel();
      throw std::invalid_argument("Wrong symbols in vertex line");
    }
  }
  if (countCoord > 3) {
    Reference_ = initDefaultModel();
    throw std::invalid_argument("Wrong amount of coordinates");
  }
};

int s21::Model::parseFacets(const char *cur, std::vector<int> &FacetsVector) {
  size_t shift = 0;
  FacetsVector.push_back(std::stoi(cur, &shift) - 1);
  return shift;
};

void s21::Model::addFacets(std::string FacetsLine) {
  std::vector<int> tempFacets;
  std::vector<int> readedFacets;
  const char *ptr = FacetsLine.c_str();
  while (*ptr) {
    if (*ptr < 33 || *ptr == 'f') {
      ++ptr;
    } else if (*ptr == '/') {
      do {
        ++ptr;
      } while (*ptr != ' ' && *ptr != '\n' && *ptr);
    } else if (isdigit(*ptr) || *ptr == '-') {
      ptr += s21::Model::parseFacets(ptr, readedFacets);
    } else {
      Reference_ = initDefaultModel();
      throw std::invalid_argument("Wrong symbols in facets line");
    }
  }
  if (readedFacets[0] < 0) {
    for (auto &t : readedFacets) {
      t = t + 1 + Reference_.verticesAmount();
    }
  }
  for (size_t t = 0; t < readedFacets.size(); ++t) {
    if (t == readedFacets.size() - 1) {
      Reference_.facets().push_back(readedFacets[0]);
      Reference_.facets().push_back(readedFacets[t]);
    } else {
      Reference_.facets().push_back(readedFacets[t]);
      Reference_.facets().push_back(readedFacets[t + 1]);
    }
  }
};

const s21::ModelData &s21::Model::readFile(std::string filename) {
  Reference_.vertices().clear();
  Reference_.facets().clear();
  std::ifstream file(filename);
  std::string line;
  if (file.is_open()) {
    while (std::getline(file, line)) {
      file >> std::ws;
      if (*(line.c_str()) == 'v' && *(line.c_str() + 1) == ' ') {
        s21::Model::addVertex(line);
      } else if (*(line.c_str()) == 'f') {
        s21::Model::addFacets(line);
      }
    }
    file.close();
  } else {
    Reference_ = initDefaultModel();
    throw std::invalid_argument("Unable to open file");
  }
  return Reference_;
};

void s21::Model::repositionReference() {
  double min_x, max_x, min_y, max_y, min_z, max_z;
  if (Reference_.verticesAmount() > 0 && Reference_.facetsSize() > 0) {
    min_x = Reference_.vertices()[0];
    max_x = Reference_.vertices()[0];
    min_y = Reference_.vertices()[1];
    max_y = Reference_.vertices()[1];
    min_z = Reference_.vertices()[2];
    max_z = Reference_.vertices()[2];
  }

  for (auto i = 0; i < Reference_.verticesAmount(); ++i) {
    if (Reference_.vertices()[i * 3] < min_x) {
      min_x = Reference_.vertices()[i * 3];
    }
    if (Reference_.vertices()[i * 3] > max_x) {
      max_x = Reference_.vertices()[i * 3];
    }
    if (Reference_.vertices()[i * 3 + 1] < min_y) {
      min_y = Reference_.vertices()[i * 3 + 1];
    }
    if (Reference_.vertices()[i * 3 + 1] > max_y) {
      max_y = Reference_.vertices()[i * 3 + 1];
    }
    if (Reference_.vertices()[i * 3 + 2] < min_z) {
      min_z = Reference_.vertices()[i * 3 + 2];
    }
    if (Reference_.vertices()[i * 3 + 2] > max_z) {
      max_z = Reference_.vertices()[i * 3 + 2];
    }
  }

  scale_ = 1.5 / fmax((max_x - min_x), (max_y - min_y));
  shiftX_ = -(max_x + min_x) * scale_ / 2;
  shiftY_ = -(max_y + min_y) * scale_ / 2;
  shiftZ_ = -(max_z + min_z) * scale_ / 2;
  transform();
  Reference_ = Current_;
  scale_ = 1;
  shiftX_ = 0;
  shiftY_ = 0;
  shiftZ_ = 0;
}

void s21::Model::setScale(double value) { scale_ = value; };

void s21::Model::setShiftX(double value) { shiftX_ = value; };

void s21::Model::setShiftY(double value) { shiftY_ = value; };

void s21::Model::setShiftZ(double value) { shiftZ_ = value; };

void s21::Model::setRotationX(double value) { rotationX_ = value; };

void s21::Model::setRotationY(double value) { rotationY_ = value; };

void s21::Model::setRotationZ(double value) { rotationZ_ = value; };

void s21::Model::MoveObject() {
  for (int i = 0; i < Current_.verticesAmount(); ++i) {
    Current_.vertices()[i * 3] = Current_.vertices()[i * 3] + shiftX_;
    Current_.vertices()[i * 3 + 1] = Current_.vertices()[i * 3 + 1] + shiftY_;
    Current_.vertices()[i * 3 + 2] = Current_.vertices()[i * 3 + 2] + shiftZ_;
  }
};

void s21::Model::RotateObject() {
  double tempX = 0, tempY = 0, tempZ = 0;
  for (int i = 0; i < Current_.verticesAmount(); ++i) {
    tempX =
        (Current_.vertices()[i * 3]) * cos(rotationZ_) * cos(rotationY_) -
        (Current_.vertices()[i * 3 + 1]) * sin(rotationZ_) * cos(rotationY_) +
        (Current_.vertices()[i * 3 + 2]) * sin(rotationY_);  // coord x
    tempY = (Current_.vertices()[i * 3]) *
                (sin(rotationX_) * sin(rotationY_) * cos(rotationZ_) +
                 sin(rotationZ_) * cos(rotationX_)) +
            (Current_.vertices()[i * 3 + 1]) *
                (-sin(rotationX_) * sin(rotationY_) * sin(rotationZ_) +
                 cos(rotationZ_) * cos(rotationX_)) -
            (Current_.vertices()[i * 3 + 2]) *
                (sin(rotationX_) * cos(rotationY_));  // coord y
    tempZ = (Current_.vertices()[i * 3]) *
                (sin(rotationX_) * sin(rotationZ_) -
                 sin(rotationY_) * cos(rotationX_) * cos(rotationZ_)) +
            (Current_.vertices()[i * 3 + 1]) *
                (sin(rotationX_) * cos(rotationZ_) -
                 sin(rotationY_) * cos(rotationX_) * sin(rotationZ_)) +
            (Current_.vertices()[i * 3 + 2]) *
                (cos(rotationX_) * cos(rotationY_));  // coord z

    Current_.vertices()[i * 3] = tempX;
    Current_.vertices()[i * 3 + 1] = tempY;
    Current_.vertices()[i * 3 + 2] = tempZ;
  }
};

void s21::Model::ScaleObject() {
  for (auto &t : Current_.vertices()) {
    t = t * scale_;
  }
}

const s21::ModelData &s21::Model::transform() {
  Current_ = Reference_;
  ScaleObject();
  MoveObject();
  RotateObject();
  return current();
}

const s21::ModelData &s21::Model::current() { return Current_; }

const s21::ModelData &s21::Model::defaultModel() { return Default_; }

s21::ModelData s21::Model::initDefaultModel() {
  ModelData newModel;
  newModel.vertices() = std::vector<double>(
      {0.500000,  -0.500000, -0.500000, 0.500000,  -0.500000, -0.500000,
       0.500000,  -0.500000, 0.500000,  -0.500000, -0.500000, 0.500000,
       -0.500000, -0.500000, -0.500000, 0.500000,  0.500000,  -0.500000,
       0.500000,  0.500000,  0.500000,  -0.500000, 0.500000,  0.500000,
       -0.500000, 0.500000,  -0.500000});
  newModel.facets() = std::vector<int>(
      {2, 3, 3, 4, 4, 2, 8, 7, 7, 6, 6, 8, 5, 6, 6, 2, 2, 5, 6, 7, 7, 3, 3, 6,
       3, 7, 7, 8, 8, 3, 1, 4, 4, 8, 8, 1, 1, 2, 2, 4, 4, 1, 5, 8, 8, 6, 6, 5,
       1, 5, 5, 2, 2, 1, 2, 6, 6, 3, 3, 2, 4, 3, 3, 8, 8, 4, 5, 1, 1, 8, 8, 5});
  return newModel;
}
