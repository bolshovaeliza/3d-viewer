#ifndef MODELDATA_H
#define MODELDATA_H

#include <vector>

namespace s21 {

/**
 * @class ModelData
 * @brief The ModelData class contains private data and public methods to access
 * it safely. Private section contains data with vertex and facets massives
 * nedeed for construction of 3D model
 */
class ModelData {
 public:
  /**
   * @brief Method return pointer to vertices massives
   */
  const double* verticesPtr() const;
  /**
   * @brief Method return pointer to facets massives
   */
  const int* facetsPtr() const;
  /**
   * @brief Method return amount of vertices
   */
  int verticesAmount() const;
  /**
   * @brief Method return amount of facets
   */
  int facetsSize() const;

  /**
   * @brief Method return reference to vertices and facets
   */
  std::vector<double>& vertices();
  /**
   * @brief Method return reference to facets
   */
  std::vector<int>& facets();

 private:
  /**
   * @brief Vector with vertices
   */
  std::vector<double> vertex_;
  /**
   * @brief Vector with facets
   */
  std::vector<int> facets_;
};

}  // namespace s21

#endif  // MODELDATA_H