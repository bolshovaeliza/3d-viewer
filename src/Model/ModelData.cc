#include "ModelData.h"

const double* s21::ModelData::verticesPtr() const { return vertex_.data(); };
const int* s21::ModelData::facetsPtr() const { return facets_.data(); };
int s21::ModelData::verticesAmount() const { return vertex_.size() / 3; };
int s21::ModelData::facetsSize() const { return facets_.size(); };

std::vector<double>& s21::ModelData::vertices() { return vertex_; };
std::vector<int>& s21::ModelData::facets() { return facets_; };
