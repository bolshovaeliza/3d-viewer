#include <gtest/gtest.h>

#include "../Model.h"

TEST(ModelDataTest, verticesPtr_test_01) {
  s21::ModelData SomeObject;
  EXPECT_EQ(SomeObject.verticesPtr(), SomeObject.vertices().data());
};

TEST(ModelDataTest, facetsPtr_test_01) {
  s21::ModelData SomeObject;
  EXPECT_EQ(SomeObject.facetsPtr(), SomeObject.facets().data());
};
