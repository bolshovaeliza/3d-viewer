#include <gtest/gtest.h>

#include "../Model.h"

TEST(ModelTest, readFile_test_01) {
  s21::Model model;
  std::string filename = "test_file.txt";
  s21::ModelData data = model.readFile(filename);
  model.repositionReference();
  EXPECT_EQ(data.verticesAmount(), 8);
  EXPECT_EQ(data.facetsSize(), 48);
};

TEST(ModelTest, readFile_test_02) {
  s21::Model model;
  std::string filename = "test_negative_cube.txt";
  s21::ModelData data = model.readFile(filename);
  EXPECT_EQ(data.verticesAmount(), 24);
  EXPECT_EQ(data.facetsSize(), 48);
};

TEST(ModelTest, readFile_test_03) {
  s21::Model model;
  EXPECT_THROW(model.readFile("filename"), std::invalid_argument);
};

TEST(ModelTest, readFile_test_04) {
  s21::Model model;
  std::string filename = "test_file_exception_01.txt";
  EXPECT_THROW(model.readFile(filename), std::invalid_argument);
};

TEST(ModelTest, readFile_test_05) {
  s21::Model model;
  std::string filename = "test_file_exception_02.txt";
  EXPECT_THROW(model.readFile(filename), std::invalid_argument);
};

TEST(ModelTest, readFile_test_06) {
  s21::Model model;
  std::string filename = "test_file_exception_03.txt";
  EXPECT_THROW(model.readFile(filename), std::invalid_argument);
};

TEST(ModelTest, setScale_test_01) {
  s21::Model CubeModel;
  CubeModel.readFile("test_file.txt");
  CubeModel.setScale(2.0);
  CubeModel.setRotationX(0.0);
  CubeModel.setRotationY(0.0);
  CubeModel.setRotationZ(0.0);
  std::vector<double> TestVertices({
      1.000000,  1.000000,  1.000000,  1.000000,  1.000000,  -1.000000,
      1.000000,  -1.000000, -1.000000, 1.000000,  -1.000000, 1.000000,
      -1.000000, -1.000000, 1.000000,  -1.000000, -1.000000, -1.000000,
      -1.000000, 1.000000,  -1.000000, -1.000000, 1.000000,  1.000000,
  });
  CubeModel.transform();
  for (int i = 0; i < CubeModel.current().verticesAmount(); ++i) {
    EXPECT_EQ(TestVertices[i], CubeModel.current().verticesPtr()[i]);
  }
};

TEST(ModelTest, setShift_test_01) {
  s21::Model CubeModel;
  CubeModel.readFile("test_file.txt");
  CubeModel.setScale(1.0);
  CubeModel.setShiftX(1.0);
  CubeModel.setShiftY(1.0);
  CubeModel.setShiftZ(1.0);
  CubeModel.setRotationX(0.0);
  CubeModel.setRotationY(0.0);
  CubeModel.setRotationZ(0.0);
  std::vector<double> TestVertices({
      1.500000, 1.500000, 1.500000, 1.500000, 1.500000, 0.500000,
      1.500000, 0.500000, 0.500000, 1.500000, 0.500000, 1.500000,
      0.500000, 0.500000, 1.500000, 0.500000, 0.500000, 0.500000,
      0.500000, 1.500000, 0.500000, 0.500000, 1.500000, 1.500000,
  });
  CubeModel.transform();
  for (int i = 0; i < CubeModel.current().verticesAmount(); ++i) {
    EXPECT_EQ(TestVertices[i], CubeModel.current().verticesPtr()[i]);
  }
};
