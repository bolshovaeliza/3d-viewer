#ifndef MODEL_H
#define MODEL_H

#include <cmath>
#include <fstream>
#include <string>
#include <vector>

#include "ModelData.h"
namespace s21 {

/**
 * @class Model
 * @brief The Model class contains most needed methods for 3D model
 * transformation. Private section contains some privatemethods: addVertex,
 * addFacets, parseCoord, ParseFacets - needed for read file data; MoveObject,
 * RotateObject, ScaleObject - needed for transform object and conditions of
 * objects in ModelData
 */
class Model {
 public:
  /**
   * @brief readFile - method for read data from file
   * @param filename - name of file with data by std::string type
   */
  const ModelData& readFile(std::string filename);

  /**
   * @brief setScale - method for set scale of object
   * @param value - value of scale
   */
  void setScale(double value);
  /**
   * @brief setShiftX - method for set shift X of object
   * @param value - value of shift X
   */
  void setShiftX(double value);
  /**
   * @brief setShiftY - method for set shift Y of object
   * @param value - value of shift Y
   */
  void setShiftY(double value);
  /**
   * @brief setShiftZ - method for set shift Z of object
   * @param value - value of shift Z
   */
  void setShiftZ(double value);
  /**
   * @brief setRotationX - method for set rotation X of object
   * @param value - value of rotation X
   */
  void setRotationX(double value);
  /**
   * @brief setRotationY - method for set rotation Y of object
   * @param value - value of rotation Y
   */
  void setRotationY(double value);
  /**
   * @brief setRotationZ - method for set rotation Z of object
   * @param value - value of rotation Z
   */
  void setRotationZ(double value);

  /**
   * @brief transform - method for transform object
   */
  const ModelData& defaultModel();
  /**
   * @brief transform - method for transform object
   */
  const ModelData& transform();
  /**
   * @brief current - getter for Current_
   */
  const ModelData& current();
  /**
   * @brief repositionReference - method for reposition Reference_
   */
  void repositionReference();

 private:
  /**
   * @brief addVertex - method for add vertex to ModelData
   * @param VerticesLine - line with vertices
   */
  void addVertex(std::string VerticesLine);
  /**
   * @brief parseCoord - method for parse coord from line
   * @param cur - line with coord
   */
  int parseCoord(const char* cur);
  /**
   * @brief addFacets - method for add facets to ModelData
   * @param FacetsLine - line with facets
   */
  void addFacets(std::string FacetsLine);
  /**
   *
   */
  int parseFacets(const char* cur, std::vector<int>& FacetsVector);

  void MoveObject();
  void RotateObject();
  void ScaleObject();
  ModelData initDefaultModel();

  /**
   * Objects ModelData Reference_ contains the default state read from
   * file
   */
  ModelData Reference_ = initDefaultModel();
  /**
   * Objects ModelData Current_ contains the state of transformation
   */
  ModelData Current_;
  /**
   * Objects ModelData Default_ contains the default state
   */
  ModelData Default_ = initDefaultModel();

  double shiftX_, shiftY_, shiftZ_;
  double rotationX_, rotationY_, rotationZ_;
  double scale_ = 1;
};

};  // namespace s21

#endif  // MODEL_H
