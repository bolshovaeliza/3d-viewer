#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <QDebug>
#include <QObject>

#include "../Model/Model.h"
#include "../Model/ModelData.h"

namespace s21 {

/**
 * \class Controller
 * \brief This objects connects to the Model object responsible for the logic of
 * the application. \author Ammoshri \details This objects provide all the
 * necessary functionality to update Model on the current changes in model
 * position (cush as scare, rotation and shift).
 */
class Controller : public QObject {
  Q_OBJECT
 public:
  /**
   * \brief Load model from file.
   * \param filepath - path to the file.
   */
  const ModelData& loadModelFromFile(QString filepath);

  /**
   * \brief Update model position.
   * \param shiftXValue - shift in x direction
   */
  const ModelData& modelShiftX(float shiftXValue);

  /**
   * \brief Update model position.
   * \param shiftYValue - shift in y direction
   */
  const ModelData& modelShiftY(float shiftYValue);

  /**
   * \brief Update model position.
   * \param shiftZValue - shift in z direction
   */
  const ModelData& modelShiftZ(float shiftZValue);

  /**
   * \brief Update model rotation.
   * \param rotationXValue - rotation around x axis
   */
  const ModelData& modelRotationX(float rotationXValue);

  /**
   * \brief Update model rotation.
   * \param rotationYValue - rotation around y axis
   */
  const ModelData& modelRotationY(float rotationYValue);

  /**
   * \brief Update model rotation.
   * \param rotationZValue - rotation around z axis
   */
  const ModelData& modelRotationZ(float rotationZValue);

  /**
   * \brief Update model scale.
   * \param scaleValue - scale
   */
  const ModelData& modelScale(float scaleValue);

  /**
   * \brief Reset model position.
   */
  const ModelData& reset();

  /**
   * \brief Get default model.
   */
  const ModelData& defaultModel();

 signals:
  /**
   * \brief Signal emitted when an exception (such as during file parsing) is
   * caught. \param what - description of the exception
   */
  void exceptionCaught(QString what);

 private:
  /**
   * \brief Model object.
   */
  Model model;
};
}  // namespace s21
#endif  // CONTROLLER_H_
