#include "Controller.h"

const s21::ModelData& s21::Controller::loadModelFromFile(QString filepath) {
  model.setShiftX(0);
  model.setShiftY(0);
  model.setShiftZ(0);
  model.setRotationX(0);
  model.setRotationY(0);
  model.setRotationZ(0);
  model.setScale(1);
  try {
    model.readFile(filepath.toStdString());
    model.repositionReference();
    return model.current();
  } catch (const std::invalid_argument& ex) {
    emit exceptionCaught(QString(ex.what()));
    return defaultModel();
  }
}

const s21::ModelData& s21::Controller::modelShiftX(float shiftXValue) {
  model.setShiftX(shiftXValue);
  return model.transform();
};
const s21::ModelData& s21::Controller::modelShiftY(float shiftYValue) {
  model.setShiftY(shiftYValue);
  return model.transform();
};
const s21::ModelData& s21::Controller::modelShiftZ(float shiftZValue) {
  model.setShiftZ(shiftZValue);
  return model.transform();
};
const s21::ModelData& s21::Controller::modelRotationX(float rotationXValue) {
  model.setRotationX(rotationXValue);
  return model.transform();
};
const s21::ModelData& s21::Controller::modelRotationY(float rotationYValue) {
  model.setRotationY(rotationYValue);
  return model.transform();
};
const s21::ModelData& s21::Controller::modelRotationZ(float rotationZValue) {
  model.setRotationZ(rotationZValue);
  return model.transform();
};
const s21::ModelData& s21::Controller::modelScale(float scaleValue) {
  model.setScale(scaleValue);
  return model.transform();
};

const s21::ModelData& s21::Controller::reset() {
  model.setShiftX(0);
  model.setShiftY(0);
  model.setShiftZ(0);
  model.setRotationX(0);
  model.setRotationY(0);
  model.setRotationZ(0);
  model.setScale(1);
  return model.transform();
}

const s21::ModelData& s21::Controller::defaultModel() {
  return model.defaultModel();
}
