#include <QApplication>

#include "MainWindow.h"

/**
 * \mainpage 3D Viewer
 *
 * \brief App for viewing wireframe 3D models.
 * \version 1.0
 */

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  s21::MainWindow window;
  window.show();
  return app.exec();
}