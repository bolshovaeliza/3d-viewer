
#ifndef SETTINGSMANAGER_H_
#define SETTINGSMANAGER_H_

#include <list>

#include "Settings.h"

namespace s21 {

/**
 * \class SettingsManager
 * \brief Manager of all settings objects across the 3D Viewer
 * \author Ammoshri
 * \details This class manages settings objects using the observer pattern.
 */
class SettingsManager : public QObject {
  Q_OBJECT
 public:
  /**
   * \brief Reference wrapper to settings object.
   */
  using SettingsRef = std::reference_wrapper<Settings>;

 public slots:
  /**
   * \brief Notifies all observers.
   * \details Calls update() on all observers.
   */
  void notify();

 public:
  /**
   * \brief Attaches an observer to the manager.
   * \param observer - object to attach.
   */
  void attach(Settings& observer);

  /**
   * \brief Detaches an observer from the manager.
   * \param observer - object to detach.
   */
  void detach(Settings& observer);

  /**
   * \brief Returns the reference settings object.
   * \return Settings& - reference to the reference settings object.
   */
  Settings& referenceSettings();
  /**
   * \brief Sets the reference settings object.
   * \param observer - object to set as reference.
   */
  void setReferenceSettings(Settings& observer);

  /**
   * \brief Sets the reference settings object.
   * \param observer - object to set as reference.
   */
  void setReferenceSettings(Settings&& observer);

 public:
  /**
   * \brief List of observers.
   */
  std::list<SettingsRef> observers;
  /**
   * \brief Reference settings object.
   */
  Settings m_referenceSettings;
};
}  // namespace s21

#endif  // SETTINGSMANAGER_H_
