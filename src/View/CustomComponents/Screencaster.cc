#include "Screencaster.h"

s21::Screencaster::Screencaster(QWidget *parent) : QWidget(parent) {
  QVBoxLayout *screencasterLayout = new QVBoxLayout(this);
  screencasterLayout->setContentsMargins(0, 0, 0, 0);

  QLabel *screencasterDescription = new QLabel("File Type", this);
  screencasterDescription->setMinimumWidth(120);
  screencasterDescription->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

  QWidget *typeChoiceWidget = new QWidget;
  QHBoxLayout *typeChoiceLayout = new QHBoxLayout(typeChoiceWidget);
  typeChoiceLayout->setContentsMargins(0, 0, 0, 0);
  QRadioButton *jpegRadioButton = new QRadioButton("JPEG", this);
  QRadioButton *bmpRadioButton = new QRadioButton("BMP", this);
  QRadioButton *gifRadioButton = new QRadioButton("GIF", this);
  typeChoiceLayout->addWidget(screencasterDescription);
  typeChoiceLayout->addWidget(jpegRadioButton);
  typeChoiceLayout->addWidget(bmpRadioButton);
  typeChoiceLayout->addWidget(gifRadioButton);
  screencasterLayout->addWidget(typeChoiceWidget);

  QPushButton *makeScreencastButton = new QPushButton("Save image");
  connect(makeScreencastButton, &QPushButton::clicked,
          [this, gifRadioButton, bmpRadioButton, jpegRadioButton]() {
            if (gifRadioButton->isChecked()) {
              startGifScreencasting();
            }
            if (bmpRadioButton->isChecked()) {
              emit makeScreencastBmp();
            }
            if (jpegRadioButton->isChecked()) {
              emit makeScreencastJpeg();
            }
          });
  screencasterLayout->addWidget(makeScreencastButton);

  QProgressBar *progressBar = new QProgressBar();
  progressBar->setRange(0, 5000);
  shortTimer = new QTimer();
  longTimer = new QTimer();

  connect(shortTimer, &QTimer::timeout, [progressBar, this]() {
    progressBar->setValue(5000 - this->longTimer->remainingTime());
    emit makeScreencastGif();
    startShortTimer();
  });

  connect(longTimer, &QTimer::timeout, this, [this, progressBar]() {
    this->shortTimer->stop();
    this->longTimer->stop();
    progressBar->setValue(5000);
    emit makeScreencastGif();
    saveGif();
    progressBar->setValue(0);
  });
  screencasterLayout->addWidget(progressBar);
}

s21::Screencaster::~Screencaster() {}

void s21::Screencaster::startLongTimer() {
  longTimer->stop();
  longTimer->start(5000);
}

void s21::Screencaster::startShortTimer() {
  shortTimer->stop();
  shortTimer->start(100);
}

QString s21::Screencaster::now() {
  QDateTime date = QDateTime::currentDateTime();
  QString formattedTime = date.toString("yyyy_MM_dd-hh_mm_ss");
  return formattedTime;
}

void s21::Screencaster::startGifScreencasting() {
  if (longTimer->isActive() == true && gifFrames != nullptr) {
    delete gifFrames;
  }
  gifFrames = new QGifImage;
  startLongTimer();
  startShortTimer();
  emit makeScreencastGif();
}

void s21::Screencaster::addGifFrame(const QImage &frame) {
  if (gifFrames != nullptr) {
    QSize newSize(640, 480);
    QImage resizedImage = frame.scaled(newSize, Qt::KeepAspectRatioByExpanding);
    gifFrames->addFrame(frame, 100);
  }
}

void s21::Screencaster::saveGif() {
  QString fileName = QFileDialog::getSaveFileName(
      this, tr("Save GIF"), now() + "_screencast.gif", tr("Images (*.gif)"));
  if (fileName != "") {
    gifFrames->save(fileName);
    delete gifFrames;
    gifFrames = nullptr;
  }
}

void s21::Screencaster::saveJpeg(const QPixmap &image) {
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save JPEG image"),
                                                  now() + "_screenshot.jpeg",
                                                  tr("Images (*.jpeg *.jpg)"));
  if (fileName != "") {
    image.save(fileName);
  }
}

void s21::Screencaster::saveBmp(const QPixmap &image) {
  QString fileName = QFileDialog::getSaveFileName(this, tr("Save BMP image"),
                                                  now() + "_screenshot.bmp",
                                                  tr("Images (*.bmp)"));
  if (fileName != "") {
    image.save(fileName);
  }
}
