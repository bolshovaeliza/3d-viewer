#ifndef MODELPOSITIONCHANGE_H_
#define MODELPOSITIONCHANGE_H_

#include <QDoubleValidator>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QWidget>

#include "FloatInputWidget.h"

namespace s21 {
/**
 * \class ModelPositionChangeMenu
 * \brief Widget that allows user to change model position.
 * \author Ammoshri
 * \details This widget seven FloatInputWidgets that allow the user to change
 * the position, rotation and scale of the model. There is also a reset button
 * that sets all input widgets to default values (0 for all except scale that is
 * 1).
 */
class ModelPositionChangeMenu : public QWidget {
  Q_OBJECT

 public:
  /** \brief Default constructor.
   * \param parent - parent widget.
   */
  ModelPositionChangeMenu(QWidget* parent = nullptr);

  /** \brief Destructor.
   */
  ~ModelPositionChangeMenu();

 signals:
  /** \brief Signal emitted when shift X value is changed.
   * \param value - new shift X value.
   */
  void shiftXChanged(double value);

  /** \brief Signal emitted when shift Y value is changed.
   * \param value - new shift Y value.
   */
  void shiftYChanged(double value);

  /** \brief Signal emitted when shift Z value is changed.
   * \param value - new shift Z value.
   */
  void shiftZChanged(double value);

  /** \brief Signal emitted when rotation X value is changed.
   * \param value - new rotation X value.
   */
  void rotationXChanged(double value);

  /** \brief Signal emitted when rotation Y value is changed.
   * \param value - new rotation Y value.
   */
  void rotationYChanged(double value);

  /** \brief Signal emitted when rotation Z value is changed.
   * \param value - new rotation Z value.
   */
  void rotationZChanged(double value);

  /** \brief Signal emitted when scale value is changed.
   * \param value - new scale value.
   */
  void scaleChanged(double value);

  /** \brief Signal emitted when reset button is clicked.
   */
  void reset();
};

}  // namespace s21

#endif  // MODELPOSITIONCHANGE_H_
