#include "FloatInputWidget.h"

s21::FloatInputWidget::FloatInputWidget(QWidget* parent)
    : QWidget(parent), extraCoefficient(4), m_unitValue(0.25) {
  m_valueInput = new QLineEdit(this);

  m_valueInput->setValidator(new QDoubleValidator());
  setValue(0);

  descriptionLabel = new QLabel("Float value: ", this);
  descriptionLabel->setMinimumWidth(80);  // TODO i don't like this

  QPushButton* incrementButton = new QPushButton(">", this);
  incrementButton->setFixedWidth(incrementButton->height());
  QPushButton* decrementButton = new QPushButton("<", this);
  decrementButton->setFixedWidth(decrementButton->height());
  QPushButton* incrementButtonExtra = new QPushButton(">>", this);
  incrementButtonExtra->setFixedWidth(incrementButtonExtra->height());
  QPushButton* decrementButtonExtra = new QPushButton("<<", this);
  decrementButtonExtra->setFixedWidth(decrementButtonExtra->height());

  QHBoxLayout* layout = new QHBoxLayout(this);
  layout->setSpacing(2);
  layout->setContentsMargins(0, 0, 0, 0);
  layout->addWidget(descriptionLabel);
  layout->addWidget(decrementButtonExtra);
  layout->addWidget(decrementButton);
  layout->addWidget(m_valueInput);
  layout->addWidget(incrementButton);
  layout->addWidget(incrementButtonExtra);

  connect(m_valueInput, &QLineEdit::textChanged, this,
          [this](QString value) { emit valueChanged(value.toFloat()); });
  connect(incrementButton, &QPushButton::clicked, this,
          &s21::FloatInputWidget::incrementValue);
  connect(decrementButton, &QPushButton::clicked, this,
          &s21::FloatInputWidget::decrementValue);
  connect(incrementButtonExtra, &QPushButton::clicked, this,
          &s21::FloatInputWidget::incrementValueExtra);
  connect(decrementButtonExtra, &QPushButton::clicked, this,
          &s21::FloatInputWidget::decrementValueExtra);
}

s21::FloatInputWidget::FloatInputWidget(QString description, QWidget* parent)
    : FloatInputWidget(parent) {
  setDescription(description);
}

s21::FloatInputWidget::FloatInputWidget(float startValue, float newUnitValue,
                                        QString description, QWidget* parent)
    : FloatInputWidget(parent) {
  unitValue() = newUnitValue;
  setValue(startValue);
  setDescription(description);
}

s21::FloatInputWidget::~FloatInputWidget(){};

void s21::FloatInputWidget::setDescription(QString description) {
  descriptionLabel->setText(description);
}

float s21::FloatInputWidget::value() { return m_valueInput->text().toFloat(); }

void s21::FloatInputWidget::setValue(float new_value) {
  QString valueString = QString::number(new_value);
  int pos;
  if (m_valueInput->validator() == nullptr ||
      m_valueInput->validator()->validate(valueString, pos) !=
          QValidator::Invalid) {
    m_valueInput->setText(valueString);
  }
}

QLineEdit& s21::FloatInputWidget::valueInput() { return *m_valueInput; }

void s21::FloatInputWidget::incrementValue() {
  setValue(value() + unitValue());
}

void s21::FloatInputWidget::decrementValue() {
  setValue(value() - unitValue());
}

void s21::FloatInputWidget::incrementValueExtra() {
  setValue(value() + unitValue() * extraCoefficient);
}

void s21::FloatInputWidget::decrementValueExtra() {
  setValue(value() - unitValue() * extraCoefficient);
}

float& s21::FloatInputWidget::unitValue() { return m_unitValue; }
