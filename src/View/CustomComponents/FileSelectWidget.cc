#include "FileSelectWidget.h"

s21::FileSelectWidget::FileSelectWidget(QWidget* parent) : QWidget(parent) {
  setLayout(new QHBoxLayout());
  layout()->setContentsMargins(0, 0, 0, 0);
  layout()->setSpacing(10);
  QPushButton* button = new QPushButton("Select File", this);
  connect(button, &QPushButton::clicked, [&]() {
    QString filePath = QFileDialog::getOpenFileName(
        this, "Select .obj File", "", "Wavefront OBJ Files (*.obj)");
    if (!filePath.isEmpty()) {
      this->setFilePath(filePath);
    }
  });
  layout()->addWidget(button);

  fileNameLabel = new QLabel("File Name: ", this);
  fileNameLabel->setFixedWidth(fileNameLabel->height() * 8);
  layout()->addWidget(fileNameLabel);

  vertexesNumberLabel = new QLabel("Vertexes: ", this);
  vertexesNumberLabel->setFixedWidth(vertexesNumberLabel->height() * 5);
  layout()->addWidget(vertexesNumberLabel);

  edgesNumberLabel = new QLabel("Edges: ", this);
  edgesNumberLabel->setFixedWidth(edgesNumberLabel->height() * 4);
  layout()->addWidget(edgesNumberLabel);

  fullPathLabel = new QLabel("Full Path: ", this);
  layout()->addWidget(fullPathLabel);
}

void s21::FileSelectWidget::setFilePath(QString filepath) {
  if (fileNameLabel != nullptr) {
    fileNameLabel->setText("File Name: " + QFileInfo(filepath).fileName());
    emit filePathChanged(filepath);
    if (fullPathLabel != nullptr) {
      fullPathLabel->setText("Full Path: " + filepath);
    }
  }
}

void s21::FileSelectWidget::setVertexNumber(long int value) {
  if (vertexesNumberLabel != nullptr) {
    vertexesNumberLabel->setText("Vertexes: " + QString::number(value));
  }
}

void s21::FileSelectWidget::setEdgeNumber(long int value) {
  if (edgesNumberLabel != nullptr) {
    edgesNumberLabel->setText("Edges: " + QString::number(value));
  }
}