#ifndef FILESELECTWIDGET_H_
#define FILESELECTWIDGET_H_

#include <QFileDialog>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QWidget>

namespace s21 {
/**
 * \class FileSelectWidget
 * \brief Widget for selecting .obj files.
 * \author Ammoshri
 * \details This widget allows user to select an .obj file and displays its
 * information. Information includes file name, number of edges, number of
 * vertexes, and full path.
 */
class FileSelectWidget : public QWidget {
  Q_OBJECT

 public:
  /** \brief Default constructor.
   * \param parent - parent widget.
   */
  FileSelectWidget(QWidget *parent = nullptr);

 signals:
  /**
   * \brief Signal emitted when file path is changed.
   * \param filepath - new file path.
   */
  void filePathChanged(QString filepath);

 public slots:
  /**
   * \brief Sets displayed number of vertices.
   * \param value - number of vertices.

   */
  void setVertexNumber(long int value);

  /**
   * \brief Sets displayed number of edges.
   * \param value - number of edges.

   */
  void setEdgeNumber(long int value);

 private slots:
  /** \brief Sets file path.
   * \param filepath - new file path.
   * Emits filePathChanged signal.
   */
  void setFilePath(QString filepath);

 private:
  /**
   * \brief Label displaying full path.
   */
  QLabel *fullPathLabel;
  /**
   * \brief Label displaying file name.
   */
  QLabel *fileNameLabel;
  /**
   * \brief Label displaying number of vertices.
   */
  QLabel *vertexesNumberLabel;
  /**
   * \brief Label displaying number of edges.
   */
  QLabel *edgesNumberLabel;
};

}  // namespace s21

#endif  // FILESELECTWIDGET_H_