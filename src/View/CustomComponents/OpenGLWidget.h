#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#ifdef __APPLE__
/* Defined before OpenGL and GLUT includes to avoid deprecation messages */
#define GL_SILENCE_DEPRECATION
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

#include <QColor>
#include <QDebug>
#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <QPaintEvent>
#include <QWidget>

#include "../../Model/ModelData.h"
#include "Settings.h"

#define RGB_MIN 1
#define RGB_MAX 255
namespace s21 {
/**
 * \class OpenGLWidget
 * \brief Widget that displays 3D model.
 * \author Ammoshri
 * \details This widget is a child widget of QOpenGLWidget and QOpenGLFunctions.
 */
class OpenGLWidget : public QOpenGLWidget, public QOpenGLFunctions {
  Q_OBJECT

 public:
  /** \brief Set displayed model.
   * \param newModel - new model.
   */
  void setLoadedModel(const s21::ModelData& newModel);

  /** \brief Set displayed model.
   * \param newModel - new model.
   */
  void setLoadedModel(s21::ModelData&& newModel);

  /** \brief Get displayed model.
   * \return ModelData model.
   */
  const s21::ModelData loadedModel();

  /** \brief Current display settings
   * \return Settings
   * This object is connected in MainWindow to a SettingsManager object inside
   * Settings widget. This allows to synchronize widget state with view display
   * settings.
   */
  s21::Settings& settings();

 public slots:
  /** \brief Set type of projection
   * \param value - new type of projection
   * This function also changes some display settings to reflect new projection
   * type.
   */
  void setIsProjectionParallel(bool value);

 protected:
  /** \brief Overriden QOpenGL initialization.
   */
  void initializeGL() override;
  /** \brief Overriden function for drawing OpenGL.
   */
  void paintGL() override;
  /** \brief Overriden function for resizing the widget.
   */
  void resizeGL(int w, int h) override;

  /**
   * \brief Function for drawing axes.
   */
  void paintAxes();

 private:
  /**
   * \brief Type of projection (parallel if true, central if false).
   */
  bool m_isProjectionParallel = true;

  /**
   * \brief Address of model data.
   */
  const s21::ModelData* m_loadedModel = nullptr;

  /**
   * \brief Current view settings
   */
  s21::Settings m_settings;
};

}  // namespace s21

#endif  // OPENGLWIDGET_H
