#include "SettingsWidget.h"

s21::SettingsWidget::SettingsWidget(QWidget *parent)
    : QWidget(parent), m_settingsManager(nullptr) {
  // SettingsManager::get().setReferenceObserver();
  SettingsSaved *saved = new SettingsSaved();
  saved->loadSettings();
  m_settingsManager = new SettingsManager();
  m_settingsManager->setReferenceSettings(saved->getSettings());
  m_settingsManager->attach(*saved);
  setLayout(new QVBoxLayout());
  layout()->setContentsMargins(0, 0, 0, 0);

  QLabel *projectionTypeDescription = new QLabel("Projection type", this);
  projectionTypeDescription->setMinimumWidth(120);
  projectionTypeDescription->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);

  QWidget *projectionTypeWidget = new QWidget;
  QHBoxLayout *projectionTypeLayout = new QHBoxLayout;
  projectionTypeWidget->setLayout(projectionTypeLayout);
  projectionTypeLayout->setContentsMargins(0, 0, 0, 0);
  QRadioButton *parallelRadioButton = new QRadioButton("Parallel", this);
  QRadioButton *centralRadioButton = new QRadioButton("Central", this);
  QButtonGroup *projectionTypeGroup = new QButtonGroup;
  projectionTypeGroup->addButton(parallelRadioButton);
  projectionTypeGroup->addButton(centralRadioButton);
  if (m_settingsManager->referenceSettings().isProjectionParallel() == true) {
    parallelRadioButton->setChecked(true);
  } else {
    centralRadioButton->setChecked(true);
  }
  connect(parallelRadioButton, &QRadioButton::toggled, this, [this]() {
    this->m_settingsManager->referenceSettings().setIsProjectionParallel(true);
  });
  connect(centralRadioButton, &QRadioButton::toggled, this, [this]() {
    this->m_settingsManager->referenceSettings().setIsProjectionParallel(false);
  });
  projectionTypeLayout->addWidget(projectionTypeDescription);
  projectionTypeLayout->addWidget(parallelRadioButton);
  projectionTypeLayout->addWidget(centralRadioButton);
  layout()->addWidget(projectionTypeWidget);
  // addSeparator();

  QWidget *lineTypeWidget = new QWidget;
  QHBoxLayout *lineTypeLayout = new QHBoxLayout;
  lineTypeWidget->setLayout(lineTypeLayout);
  lineTypeWidget->setContentsMargins(0, 0, 0, 0);
  QLabel *lineTypeDescription = new QLabel("Line Type", this);
  lineTypeDescription->setMinimumWidth(120);
  lineTypeDescription->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
  QRadioButton *solidRadioButton = new QRadioButton("Solid", this);
  QRadioButton *dashedRadioButton = new QRadioButton("Dashed", this);
  QButtonGroup *lineTypeGroup = new QButtonGroup;
  lineTypeGroup->addButton(solidRadioButton);
  lineTypeGroup->addButton(dashedRadioButton);
  if (m_settingsManager->referenceSettings().isLineSolid() == true) {
    solidRadioButton->setChecked(true);
  } else {
    dashedRadioButton->setChecked(true);
  }
  connect(solidRadioButton, &QRadioButton::toggled, this, [this]() {
    m_settingsManager->referenceSettings().setIsLineSolid(true);
  });
  connect(dashedRadioButton, &QRadioButton::toggled, this, [this]() {
    m_settingsManager->referenceSettings().setIsLineSolid(false);
  });
  lineTypeLayout->addWidget(lineTypeDescription);
  lineTypeLayout->addWidget(solidRadioButton);
  lineTypeLayout->addWidget(dashedRadioButton);
  layout()->addWidget(lineTypeWidget);

  s21::FloatInputWidget *lineWidthWidget =
      new s21::FloatInputWidget(0, 0.25, "Line Width ");
  lineWidthWidget->setValue(m_settingsManager->referenceSettings().lineWidth());
  lineWidthWidget->valueInput().setValidator(new QDoubleValidator(0, 100, 4));
  connect(lineWidthWidget, &s21::FloatInputWidget::valueChanged, this,
          [this](float value) {
            m_settingsManager->referenceSettings().setLineWidth(value);
          });
  layout()->addWidget(lineWidthWidget);

  QWidget *lineColorWidget = new QWidget;
  QHBoxLayout *lineColorLayout = new QHBoxLayout(lineColorWidget);
  lineColorLayout->setContentsMargins(0, 0, 0, 0);
  QLineEdit *lineColorEdit =
      new QLineEdit(m_settingsManager->referenceSettings().lineColor().name());
  lineColorEdit->setReadOnly(true);
  QPushButton *lineColorButton = new QPushButton("Select Line Color");
  QObject::connect(
      lineColorButton, &QPushButton::clicked, [lineColorEdit, this]() {
        QColor color = QColorDialog::getColor(
            this->m_settingsManager->referenceSettings().lineColor());
        if (color.isValid()) {
          QString colorText = color.name();
          lineColorEdit->setText(colorText);
          m_settingsManager->referenceSettings().setLineColor(color);
        }
      });
  lineColorLayout->addWidget(lineColorEdit);
  lineColorLayout->addWidget(lineColorButton);
  layout()->addWidget(lineColorWidget);
  // addSeparator();

  QWidget *verticesDisplayWidget = new QWidget;
  QHBoxLayout *verticesDisplayLayout = new QHBoxLayout(verticesDisplayWidget);
  verticesDisplayLayout->setContentsMargins(0, 0, 0, 0);
  QLabel *verticesDisplayDescription = new QLabel("Dot Display", this);
  verticesDisplayDescription->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
  QRadioButton *noneRadioButton = new QRadioButton("None", this);
  QRadioButton *circleRadioButton = new QRadioButton("Circle", this);
  QRadioButton *squareRadioButton = new QRadioButton("Square", this);
  QButtonGroup *verticesDisplay = new QButtonGroup;
  if (m_settingsManager->referenceSettings().isVerticesVisible() == true) {
    if (m_settingsManager->referenceSettings().isVerticesVisible() == true)
      circleRadioButton->setChecked(true);
    else
      squareRadioButton->setChecked(true);
  } else {
    noneRadioButton->setChecked(true);
  }
  connect(noneRadioButton, &QRadioButton::toggled, this, [this]() {
    m_settingsManager->referenceSettings().setIsVerticesVisible(false);
    m_settingsManager->referenceSettings().setIsVerticesCircle(false);
  });
  connect(circleRadioButton, &QRadioButton::toggled, this, [this]() {
    m_settingsManager->referenceSettings().setIsVerticesVisible(true);
    m_settingsManager->referenceSettings().setIsVerticesCircle(true);
  });
  connect(squareRadioButton, &QRadioButton::toggled, this, [this]() {
    m_settingsManager->referenceSettings().setIsVerticesVisible(true);
    m_settingsManager->referenceSettings().setIsVerticesCircle(false);
  });
  verticesDisplay->addButton(noneRadioButton);
  verticesDisplay->addButton(circleRadioButton);
  verticesDisplay->addButton(squareRadioButton);
  verticesDisplayLayout->addWidget(verticesDisplayDescription);
  verticesDisplayLayout->addWidget(noneRadioButton);
  verticesDisplayLayout->addWidget(circleRadioButton);
  verticesDisplayLayout->addWidget(squareRadioButton);
  layout()->addWidget(verticesDisplayWidget);

  s21::FloatInputWidget *verticesSizeWidget =
      new s21::FloatInputWidget(0, 0.25, "Vertices Size ");
  verticesSizeWidget->setValue(
      m_settingsManager->referenceSettings().verticesSize());
  verticesSizeWidget->valueInput().setValidator(
      new QDoubleValidator(0, 100, 4));
  connect(verticesSizeWidget, &s21::FloatInputWidget::valueChanged, this,
          [this](float value) {
            m_settingsManager->referenceSettings().setVerticesSize(value);
          });
  layout()->addWidget(verticesSizeWidget);

  QWidget *verticeColorWidget = new QWidget;
  QHBoxLayout *verticeColorLayout = new QHBoxLayout(verticeColorWidget);
  verticeColorLayout->setContentsMargins(0, 0, 0, 0);
  QLineEdit *verticeColorEdit = new QLineEdit(
      m_settingsManager->referenceSettings().verticesColor().name());
  verticeColorEdit->setReadOnly(true);
  QPushButton *verticeColorButton = new QPushButton("Select Vert. Color");
  QObject::connect(
      verticeColorButton, &QPushButton::clicked, [verticeColorEdit, this]() {
        QColor color = QColorDialog::getColor(
            this->m_settingsManager->referenceSettings().verticesColor());
        if (color.isValid()) {
          QString colorText = color.name();
          verticeColorEdit->setText(colorText);
          m_settingsManager->referenceSettings().setVerticesColor(color);
        }
      });
  verticeColorLayout->addWidget(verticeColorEdit);
  verticeColorLayout->addWidget(verticeColorButton);
  layout()->addWidget(verticeColorWidget);

  QWidget *backgroundColorWidget = new QWidget;
  QHBoxLayout *backgroundColorLayout = new QHBoxLayout(backgroundColorWidget);
  backgroundColorLayout->setContentsMargins(0, 0, 0, 0);
  QLineEdit *backgroundColorEdit = new QLineEdit(
      m_settingsManager->referenceSettings().backgroundColor().name());
  backgroundColorEdit->setReadOnly(true);
  QPushButton *backgroundColorButton = new QPushButton("Select Back. Color");
  QObject::connect(
      backgroundColorButton, &QPushButton::clicked,
      [backgroundColorEdit, this]() {
        QColor color = QColorDialog::getColor(
            this->m_settingsManager->referenceSettings().backgroundColor());
        if (color.isValid()) {
          QString colorText = color.name();
          backgroundColorEdit->setText(colorText);
          m_settingsManager->referenceSettings().setBackgroundColor(color);
        } else {
          qDebug() << "color is invalid";
        }
      });
  backgroundColorLayout->addWidget(backgroundColorEdit);
  backgroundColorLayout->addWidget(backgroundColorButton);
  layout()->addWidget(backgroundColorWidget);
}

s21::SettingsWidget::~SettingsWidget() { delete m_settingsManager; }

s21::SettingsManager &s21::SettingsWidget::settingsManager() {
  return *m_settingsManager;
}
