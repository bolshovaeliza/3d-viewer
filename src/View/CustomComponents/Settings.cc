#include "Settings.h"

s21::Settings::Settings() {}

s21::Settings::Settings(const s21::Settings &other) { *this = other; }

s21::Settings::Settings(s21::Settings &&other) { *this = std::move(other); }

s21::Settings::~Settings() {}

s21::Settings &s21::Settings::operator=(const Settings &other) {
  setIsProjectionParallel(other.isProjectionParallel());
  setLineWidth(other.lineWidth());
  setIsLineSolid(other.isLineSolid());
  setLineColor(other.lineColor());
  setIsVerticesVisible(other.isVerticesVisible());
  setIsVerticesCircle(other.isVerticesCircle());
  setVerticesSize(other.verticesSize());
  setVerticesColor(other.verticesColor());
  setBackgroundColor(other.backgroundColor());
  return *this;
}

s21::Settings &s21::Settings::operator=(Settings &&other) {
  setIsProjectionParallel(other.isProjectionParallel());
  setLineWidth(other.lineWidth());
  setIsLineSolid(other.isLineSolid());
  setLineColor(other.lineColor());
  setIsVerticesVisible(other.isVerticesVisible());
  setIsVerticesCircle(other.isVerticesCircle());
  setVerticesSize(other.verticesSize());
  setVerticesColor(other.verticesColor());
  setBackgroundColor(other.backgroundColor());
  return *this;
}

void s21::Settings::update(s21::Settings &settings) { *this = settings; }

void s21::Settings::setIsProjectionParallel(bool value) {
  m_isProjectionParallel = value;
  emit settingsChanged();
}
void s21::Settings::setLineWidth(float value) {
  m_lineWidth = value;
  emit settingsChanged();
}
void s21::Settings::setIsLineSolid(bool value) {
  m_isLineSolid = value;
  emit settingsChanged();
}
void s21::Settings::setLineColor(QColor value) {
  m_lineColor = value;
  emit settingsChanged();
}
void s21::Settings::setIsVerticesVisible(bool value) {
  m_isVerticesVisible = value;
  emit settingsChanged();
}
void s21::Settings::setIsVerticesCircle(bool value) {
  m_isVerticesCircle = value;
  emit settingsChanged();
}
void s21::Settings::setVerticesSize(float value) {
  m_verticesSize = value;
  emit settingsChanged();
}
void s21::Settings::setVerticesColor(QColor value) {
  m_verticesColor = value;
  emit settingsChanged();
}
void s21::Settings::setBackgroundColor(QColor value) {
  m_backgroundColor = value;
  emit settingsChanged();
}

bool s21::Settings::isProjectionParallel() const {
  return m_isProjectionParallel;
}
float s21::Settings::lineWidth() const { return m_lineWidth; }
bool s21::Settings::isLineSolid() const { return m_isLineSolid; }
QColor s21::Settings::lineColor() const { return m_lineColor; }
bool s21::Settings::isVerticesVisible() const { return m_isVerticesVisible; }
bool s21::Settings::isVerticesCircle() const { return m_isVerticesCircle; }
float s21::Settings::verticesSize() const { return m_verticesSize; }
QColor s21::Settings::verticesColor() const { return m_verticesColor; }
QColor s21::Settings::backgroundColor() const { return m_backgroundColor; }
