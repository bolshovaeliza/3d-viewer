#include "ModelPositionChangeMenu.h"

s21::ModelPositionChangeMenu::ModelPositionChangeMenu(QWidget* parent)
    : QWidget(parent) {
  setLayout(new QVBoxLayout());

  s21::FloatInputWidget* shiftX = new s21::FloatInputWidget("Shift X:");
  layout()->addWidget(shiftX);
  layout()->setContentsMargins(0, 0, 0, 0);
  shiftX->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  connect(shiftX, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::shiftXChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, shiftX]() {
    this->blockSignals(true);
    shiftX->setValue(0);
    this->blockSignals(false);
  });

  s21::FloatInputWidget* shiftY = new s21::FloatInputWidget("Shift Y:");
  layout()->addWidget(shiftY);
  shiftY->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  connect(shiftY, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::shiftYChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, shiftY]() {
    this->blockSignals(true);
    shiftY->setValue(0);
    this->blockSignals(false);
  });

  s21::FloatInputWidget* shiftZ = new s21::FloatInputWidget("Shift Z:");
  layout()->addWidget(shiftZ);
  shiftZ->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  connect(shiftZ, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::shiftZChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, shiftZ]() {
    this->blockSignals(true);
    shiftZ->setValue(0);
    this->blockSignals(false);
  });

  s21::FloatInputWidget* rotationX = new s21::FloatInputWidget("Rotation X:");
  layout()->addWidget(rotationX);
  rotationX->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  connect(rotationX, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::rotationXChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, rotationX]() {
    this->blockSignals(true);
    rotationX->setValue(0);
    this->blockSignals(false);
  });

  s21::FloatInputWidget* rotationY = new s21::FloatInputWidget("Rotation Y:");
  layout()->addWidget(rotationY);
  rotationY->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  connect(rotationY, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::rotationYChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, rotationY]() {
    this->blockSignals(true);
    rotationY->setValue(0);
    this->blockSignals(false);
  });

  s21::FloatInputWidget* rotationZ = new s21::FloatInputWidget("Rotation Z:");
  layout()->addWidget(rotationZ);
  rotationZ->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  connect(rotationZ, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::rotationZChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, rotationZ]() {
    this->blockSignals(true);
    rotationZ->setValue(0);
    this->blockSignals(false);
  });

  s21::FloatInputWidget* scale = new s21::FloatInputWidget("Scale:");
  scale->setValue(1);
  layout()->addWidget(scale);
  scale->valueInput().setValidator(new QDoubleValidator(-100, 100, 4));
  scale->unitValue() = 0.125;
  connect(scale, &s21::FloatInputWidget::valueChanged, this,
          &s21::ModelPositionChangeMenu::scaleChanged);
  connect(this, &s21::ModelPositionChangeMenu::reset, [this, scale]() {
    this->blockSignals(true);
    scale->setValue(1);
    this->blockSignals(false);
  });

  QPushButton* resetButton = new QPushButton("Reset");
  layout()->addWidget(resetButton);
  connect(resetButton, &QPushButton::clicked, this,
          &s21::ModelPositionChangeMenu::reset);
}

s21::ModelPositionChangeMenu::~ModelPositionChangeMenu(){};
