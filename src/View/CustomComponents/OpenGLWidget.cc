#include "OpenGLWidget.h"

void s21::OpenGLWidget::setIsProjectionParallel(bool value) {
  if (value == true && m_isProjectionParallel == false) {
    glTranslatef(0, 0, 4.3);
  }
  if (value == false && m_isProjectionParallel == true) {
    glTranslatef(0, 0, -4.3);
  }
  m_isProjectionParallel = value;
}

void s21::OpenGLWidget::initializeGL() {
  initializeOpenGLFunctions();
  glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
  glEnable(GL_COLOR_MATERIAL);
}

void s21::OpenGLWidget::paintGL() {
  // PROJECTION ---------------------------------------------------------------

  GLdouble ratioW, ratioH;
  if (width() > height()) {
    ratioW = (double)width() / (double)height();
    ratioH = 1;
  } else {
    ratioW = 1;
    ratioH = (double)height() / (double)width();
  }

  if (settings().isProjectionParallel() == true) {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-ratioW, ratioW, -ratioH, ratioH, -10, 10);
    glMatrixMode(GL_MODELVIEW);
    setIsProjectionParallel(true);
  } else {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ratioW, ratioW, -ratioH, ratioH, 2, 100);
    glMatrixMode(GL_MODELVIEW);
    setIsProjectionParallel(false);
  }

  // BACKGROUND ---------------------------------------------------------------
  QColor colorBackground = settings().backgroundColor();
  glClearColor(colorBackground.redF(), colorBackground.greenF(),
               colorBackground.blueF(), colorBackground.alphaF());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  paintAxes();

  // VERTICES -----------------------------------------------------------------
  glEnableClientState(GL_VERTEX_ARRAY);
  if (settings().isVerticesCircle()) {
    glEnable(GL_POINT_SMOOTH);  // не работает на линуксе
    glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
  } else {
    glDisable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
  }

  if (settings().isVerticesVisible()) {
    glPointSize(settings().verticesSize());
  } else {
    glPointSize(0.0001);
  }

  QColor colorVertex = settings().verticesColor();
  glColor3f(colorVertex.redF(), colorVertex.greenF(), colorVertex.blueF());

  if (loadedModel().verticesAmount() > 0) {
    glVertexPointer(3, GL_DOUBLE, 0, loadedModel().verticesPtr());
    glDrawArrays(GL_POINTS, 0, loadedModel().verticesAmount());
  }

  // EDGES --------------------------------------------------------------------
  // тип линий
  if (settings().isLineSolid()) {
    glDisable(GL_LINE_STIPPLE);
  } else {
    glEnable(GL_LINE_STIPPLE);
  }
  // устанавливаем цвет
  glLineWidth(settings().lineWidth());
  QColor colorLine = settings().lineColor();
  glColor3f(colorLine.redF(), colorLine.greenF(), colorLine.blueF());

  if (loadedModel().facetsSize() > 0) {
    glDrawElements(GL_LINES, loadedModel().facetsSize(), GL_UNSIGNED_INT,
                   loadedModel().facetsPtr());
  }

  glDisableClientState(GL_VERTEX_ARRAY);
}

void s21::OpenGLWidget::setLoadedModel(const s21::ModelData& newModel) {
  if (m_loadedModel != nullptr) {
    delete m_loadedModel;
  }
  m_loadedModel = new s21::ModelData(newModel);
  update();
}

void s21::OpenGLWidget::setLoadedModel(s21::ModelData&& newModel) {
  if (m_loadedModel != nullptr) {
    delete m_loadedModel;
  }
  m_loadedModel = new s21::ModelData(newModel);
  update();
}

const s21::ModelData s21::OpenGLWidget::loadedModel() { return *m_loadedModel; }

s21::Settings& s21::OpenGLWidget::settings() { return m_settings; }

void s21::OpenGLWidget::resizeGL(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
}

void s21::OpenGLWidget::paintAxes() {
  glEnable(GL_LINE_STIPPLE);
  glLineStipple(50, 0x3333);
  glLineWidth(1.0);
  glBegin(GL_LINES);
  glColor3f(0.5, 0.4, 0.4);
  glVertex3f(0.0f, -1000.0f, 0.0f);
  glVertex3f(0.0f, 1000.0f, 0.0f);

  glColor3f(0.4, 0.5, 0.4);
  glVertex3f(0.0f, 0.0f, -1000.0f);
  glVertex3f(0.0f, 0.0f, 1000.0f);

  glColor3f(0.4, 0.4, 0.5);
  glVertex3f(-1000.0f, 0.0f, 0.0f);
  glVertex3f(1000.0f, 0.0f, .0f);
  glEnd();
  glDisable(GL_LINE_STIPPLE);
}
