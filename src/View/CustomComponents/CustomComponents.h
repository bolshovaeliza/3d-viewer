#ifndef CUSTOMCOMPONENTS_H
#define CUSTOMCOMPONENTS_H

#include "FileSelectWidget.h"
#include "FloatInputWidget.h"
#include "ModelPositionChangeMenu.h"
#include "OpenGLWidget.h"
#include "Screencaster.h"
#include "Settings.h"
#include "SettingsManager.h"
#include "SettingsSaved.h"
#include "SettingsWidget.h"

#endif  // CUSTOMCOMPONENTS_H