#ifndef FLOATINPUTWIDGET_H_
#define FLOATINPUTWIDGET_H_

#include <QDoubleValidator>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QWidget>

namespace s21 {

/**
 * \class FloatInputWidget
 * \brief Widget that allows input of floating point values with additional
 * buttons.
 * \author Ammoshri
 * \details This widget consists of value input field,
 * description label, and four buttons that can be used to increment and
 * decrement value by one unit or multiple units at a time.
 */
class FloatInputWidget : public QWidget {
  Q_OBJECT

 public:
  /** \brief Default constructor.
   * \param parent - parent widget.
   */
  FloatInputWidget(QWidget* parent = nullptr);

  /** \brief Parametrized constructor.
   * \param startValue - initial value.
   * \param unitValue - unit value.
   * \param description - description of the value.
   * \param parent - parent widget.
   */
  FloatInputWidget(float startValue, float unitValue, QString description,
                   QWidget* parent = nullptr);

  /** \brief Parametrized constructor.
   * \param description - description of the value.
   * \param parent - parent widget.
   */
  FloatInputWidget(QString description, QWidget* parent = nullptr);

  /** \brief Destructor.
   */
  ~FloatInputWidget();

 signals:
  /** \brief Signal emitted when value is changed.
   * \param value - new value.
   */
  void valueChanged(float value);

 public slots:
  /** \brief Value getter
   * \return Current value of the widget.
   */
  float value();

  /** \brief Description setter
   * \param description - new description.

   */
  void setDescription(QString description);

  /** \brief Value setter
   * \param new_value - new value.

   * Emits valueChanged signal.
   */
  void setValue(float new_value);

  /** \brief Value input widget getter
   * \return Value input widget.
   */
  QLineEdit& valueInput();

  /**
   * \brief Unit value accessor.
   * \return Unit value.
   */
  float& unitValue();

 private slots:
  /**
   * \brief Increment value by unit.

   */
  void incrementValue();

  /**
   * \brief Decrement value by unit.

   */
  void decrementValue();

  /**
   * \brief Increment value by four units.
   */
  void incrementValueExtra();

  /**
   * \brief Decrement value by four units.
   */
  void decrementValueExtra();

 private:
  /**
   * \brief Value input widget.
   */
  QLineEdit* m_valueInput;
  /**
   * \brief Label displaying value description.
   */
  QLabel* descriptionLabel;
  /**
   * \brief Number of units by which the value will be changed
   * when pressing some of the buttons.
   */
  int extraCoefficient;
  /**
   * \brief Size of increment and decrement unit.
   */
  float m_unitValue;
};

}  // namespace s21

#endif  // FLOATINPUTWIDGET_H_