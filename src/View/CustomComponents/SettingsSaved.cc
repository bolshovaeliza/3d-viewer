#include "SettingsSaved.h"

void s21::SettingsSaved::update(s21::Settings& settings) {
  setIsProjectionParallel(settings.isProjectionParallel());
  setLineWidth(settings.lineWidth());
  setIsLineSolid(settings.isLineSolid());
  setLineColor(settings.lineColor());
  setIsVerticesVisible(settings.isVerticesVisible());
  setIsVerticesCircle(settings.isVerticesCircle());
  setVerticesSize(settings.verticesSize());
  setVerticesColor(settings.verticesColor());
  setBackgroundColor(settings.backgroundColor());
  saveSettings();
}

void s21::SettingsSaved::loadSettings() {
  if (viewerSettings == nullptr) {
    viewerSettings = new QSettings("AMMOSHRI Assc. & Conf.", "3DViewer_v1.0");
  }
  m_isProjectionParallel =
      viewerSettings->value("isProjectionParallel").toBool();
  m_isLineSolid = viewerSettings->value("isLineSolid").toBool();
  m_lineWidth = viewerSettings->value("lineWidth").toFloat();
  if (m_lineWidth < 1) {
    m_lineWidth = 1;
  }

  m_lineColor = QColor(viewerSettings->value("lineColor").toString());
  if (m_lineColor.isValid() == false) {
    m_lineColor = QColor("#208fff");
  }
  m_isVerticesVisible = viewerSettings->value("isVerticesVisible").toBool();
  m_isVerticesCircle = viewerSettings->value("isVerticesCircle").toBool();
  m_verticesSize = viewerSettings->value("verticesSize").toFloat();
  if (m_verticesSize < 1) {
    m_verticesSize = 1;
  }
  m_verticesColor = QColor(viewerSettings->value("verticesColor").toString());

  if (m_verticesColor.isValid() == false) {
    m_verticesColor = QColor("#208fff");
  }
  m_backgroundColor =
      QColor(viewerSettings->value("backgroundColor").toString());
  if (m_backgroundColor.isValid() == false) {
    m_backgroundColor = QColor("#737373");
  }
}

s21::Settings s21::SettingsSaved::getSettings() {
  s21::Settings newSettings;
  newSettings.setIsProjectionParallel(isProjectionParallel());
  newSettings.setLineWidth(lineWidth());
  newSettings.setIsLineSolid(isLineSolid());
  newSettings.setLineColor(lineColor());
  newSettings.setIsVerticesVisible(isVerticesVisible());
  newSettings.setIsVerticesCircle(isVerticesCircle());
  newSettings.setVerticesSize(verticesSize());
  newSettings.setVerticesColor(verticesColor());
  newSettings.setBackgroundColor(backgroundColor());
  return newSettings;
}

void s21::SettingsSaved::saveSettings() {
  if (viewerSettings == nullptr) {
    viewerSettings = new QSettings("AMMOSHRI Assc. & Conf.", "3DViewer_v1.0");
  }
  // qDebug() << viewerSettings->allKeys();
  // qDebug() << verticesColor().name();
  viewerSettings->setValue("isProjectionParallel", isProjectionParallel());
  viewerSettings->setValue("isLineSolid", isLineSolid());
  viewerSettings->setValue("lineWidth", lineWidth());
  viewerSettings->setValue("lineColor", lineColor().name());
  viewerSettings->setValue("isVerticesVisible", isVerticesVisible());
  viewerSettings->setValue("isVerticesCircle", isVerticesCircle());
  viewerSettings->setValue("verticesSize", verticesSize());
  viewerSettings->setValue("verticesColor", verticesColor().name());
  viewerSettings->setValue("backgroundColor", backgroundColor().name());
}
