find_package(Qt6 COMPONENTS ${QT_MODULES} REQUIRED)

add_library(QtGifImage
            giflib/dgif_lib.c 
            giflib/egif_lib.c 
            giflib/gif_err.c
            giflib/gif_font.c 
            giflib/gif_hash.c
            giflib/gifalloc.c
            giflib/quantize.c
            gifimage/qgifimage.cpp
            
           
)

target_link_libraries(QtGifImage 
                        PUBLIC
                        ${QT_PREFIXED_MODULES}
                        )


target_include_directories(QtGifImage
                            INTERFACE
                            .)