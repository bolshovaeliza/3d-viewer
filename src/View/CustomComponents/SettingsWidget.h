#ifndef SETTINGSWIDGET_H_
#define SETTINGSWIDGET_H_

#include <QButtonGroup>
#include <QColorDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QRadioButton>
#include <QVBoxLayout>
#include <QWidget>

#include "FloatInputWidget.h"
#include "SettingsManager.h"
#include "SettingsSaved.h"

namespace s21 {
/**
 * \class SettingsWidget
 * \brief Widget that allows user to change viewer settings.
 * \author Ammoshri
 * \details This widget has widgets that allows user to change settings. All
 * changes are saved in SettingsManager and then get translated to all
 * observers.
 *
 */
class SettingsWidget : public QWidget {
  Q_OBJECT

 public:
  /**
   * \brief Default constructor.
   * \param parent - parent widget.
   */
  SettingsWidget(QWidget* parent = nullptr);
  /**
   * \brief Destructor.
   */
  ~SettingsWidget();
  /**
   * \brief Set settings manager.
   * \param settingsManager - settings manager.
   */
  SettingsManager& settingsManager();

 private:
  /**
   * \brief Settings manager.
   * \see SettingsManager
   */
  SettingsManager* m_settingsManager;
};

}  // namespace s21

#endif  // SETTINGSWIDGET_H_
