#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QColor>
#include <QDebug>
#include <QObject>

namespace s21 {
/**
 * \class Settings
 * \brief Objects that contains all settings of the view.
 * \author Ammoshri
 * \details This class is a child of QObject and is responcible for storing
 * settings, as well as updating them when called by the SettingsManager.
 */
class Settings : public QObject {
  Q_OBJECT
 public:
  // CONTRUCTORS AND DSTRUCTORS -----------------------------------------------
  /**
   * \brief Default constructor.
   */
  Settings();

  /**
   * \brief Copy constructor.
   * \param other - object to copy.
   */
  Settings(const Settings &other);

  /**
   * \brief Move constructor.
   * \param other - object to move.
   */
  Settings(Settings &&other);

  /**
   * \brief Copy assignment operator.
   * \param other - object to copy.
   * \return Settings& - reference to the copied object.
   */
  Settings &operator=(Settings &&other);

  /**
   * \brief Copy assignment operator.
   * \param other - object to copy.
   * \return Settings& - reference to the copied object.
   */
  Settings &operator=(const Settings &other);

  /**
   * \brief Destructor.
   */
  ~Settings();

  // GETTERS ------------------------------------------------------------------

  /**
   * \brief Returns if projection is parallel or not.
   * \return bool - true if projection is parallel, false if not.
   */
  bool isProjectionParallel() const;

  /**
   * \brief Returns line width.
   * \return float - line width.
   */
  float lineWidth() const;

  /**
   * \brief Returns if line is solid or not.
   * \return bool - true if line is solid, false if not.
   */
  bool isLineSolid() const;

  /**
   * \brief Returns line color.
   * \return QColor - line color.
   */
  QColor lineColor() const;

  /**
   * \brief Returns if vertices are visible or not.
   * \return bool - true if vertices are visible, false if not.
   */
  bool isVerticesVisible() const;

  /**
   * \brief Returns if vertices are circles or not.
   * \return bool - true if vertices are circles, false if squares.
   */
  bool isVerticesCircle() const;

  /**
   * \brief Returns vertices size.
   * \return float - vertices size.
   */
  float verticesSize() const;

  /**
   * \brief Returns vertices color.
   * \return QColor - vertices color.
   */
  QColor verticesColor() const;

  /**
   * \brief Returns background color.
   * \return QColor - background color.
   */
  QColor backgroundColor() const;

  /**
   * \brief Update settings.
   * \param settings - new settings.
   */
  virtual void update(Settings &settings);

  // SETTERS ------------------------------------------------------------------

  /**
   * \brief Set type of projection
   * \param value - new type of projection
   */
  void setIsProjectionParallel(bool value);

  /**
   * \brief Set line width
   * \param value - new line width
   */
  void setLineWidth(float value);

  /**
   * \brief Set if line is solid
   * \param value - new if line is solid
   */
  void setIsLineSolid(bool value);

  /**
   * \brief Set line color
   * \param value - new line color
   */
  void setLineColor(QColor value);

  /**
   * \brief Set if vertices are visible
   * \param value - new if vertices are visible
   */
  void setIsVerticesVisible(bool value);

  /**
   * \brief Set if vertices are circles
   * \param value - new if vertices are circles
   */
  void setIsVerticesCircle(bool value);

  /**
   * \brief Set vertices size
   * \param value - new vertices size
   */
  void setVerticesSize(float value);

  /**
   * \brief Set vertices color
   * \param value - new vertices color
   */
  void setVerticesColor(QColor value);

  /**
   * \brief Set background color
   * \param value - new background color
   */
  void setBackgroundColor(QColor value);

 signals:

  /**
   * \brief Signal emitted when settings changed
   */
  void settingsChanged();

 protected:
  /**
   * \brief If projection is parallel or not.
   */
  bool m_isProjectionParallel;

  /**
   * \brief Line width.
   */
  float m_lineWidth;

  /**
   * \brief If line is solid or dashed.
   */
  bool m_isLineSolid;

  /**
   * \brief Line color.
   */
  QColor m_lineColor;

  /**
   * \brief If vertices are visible or not.
   */
  bool m_isVerticesVisible;

  /**
   * \brief If vertices are circles or squares.
   */
  bool m_isVerticesCircle;

  /**
   * \brief Vertices size.
   */
  float m_verticesSize;

  /**
   * \brief Vertices color.
   */
  QColor m_verticesColor;

  /**
   * \brief Background color.
   */
  QColor m_backgroundColor;
};

}  // namespace s21

#endif  // SETTINGS_H_
