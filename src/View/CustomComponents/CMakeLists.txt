find_package(Qt6 COMPONENTS ${QT_MODULES} REQUIRED)

add_library(CustomComponents
            FloatInputWidget.cc
            FileSelectWidget.cc
            ModelPositionChangeMenu.cc
            OpenGLWidget.cc
            Screencaster.cc
            SettingsWidget.cc
            Settings.cc 
            SettingsManager.cc 
            SettingsSaved.cc
)

add_subdirectory(QtGifImage)

target_link_libraries(CustomComponents 
                        PRIVATE
                        QtGifImage
                        ${QT_PREFIXED_MODULES}
                        )

