#ifndef SCREENCASTER_H_
#define SCREENCASTER_H_

#include <QDebug>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
#include <QRadioButton>
#include <QSize>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>

#include "QtGifImage/gifimage/qgifimage.h"

namespace s21 {
/**
 * \class Screencaster
 * \brief Widget that allows to create screencasts of currently displayed model.
 * \author Ammoshri
 * \details This widget allows to create .jped, .bmp, and .gif files from
 * currently displayed model. it has a group of radio buttons that can be used
 * to select the type of screencast. GIFs are made with 10 fps during 5 seconds.
 * Current progress is displayed in a progress bar. After the the button is
 * pressed (or timer for gif casting runs out), file dialog is opened and the
 * file is saved.
 */
class Screencaster : public QWidget {
  Q_OBJECT

 public:
  /**
   * \brief Default constructor.
   * \param parent - parent widget.
   */
  Screencaster(QWidget *parent = nullptr);
  /**
   * \brief Destructor.
   */
  ~Screencaster();

 signals:
  /**
   * \brief Signal emitted when frame for .jpeg screencast is requested
   */
  void makeScreencastJpeg();

  /**
   * \brief Signal emitted when frame for .bmp screencast is requested
   */
  void makeScreencastBmp();

  /**
   * \brief Signal emitted when frame for .gif screencast is requested
   */
  void makeScreencastGif();

 public slots:
  /**
   * \brief Adds a frame to the gif that is being created
   * \param frame - new frame
   */
  void addGifFrame(const QImage &frame);

  /**
   * \brief Saves a frame in jpeg
   * \param image - image to save
   */
  void saveJpeg(const QPixmap &image);

  /**
   * \brief Saves a frame in bmp
   * \param image - image to save
   */
  void saveBmp(const QPixmap &image);

 private slots:

  /**
   * \brief Starts a 5 sec timer (duration of gif casting)
   */
  void startLongTimer();

  /**
   * \brief Starts a short timer that determines time between gif frames
   */
  void startShortTimer();

  /**
   * \brief Starts a gif screencasting
   */
  void startGifScreencasting();

  /**
   * \brief Saves a gif
   */
  void saveGif();

  /**
   * \brief Returns current time in format "yyyy_MM_dd-hh_mm_ss"
   * \return current time
   */
  QString now();

 private:
  /**
   * \brief Short timer (time between gif frames)
   */
  QTimer *shortTimer;
  /**
   * \brief Long timer (time of gif casting)
   */
  QTimer *longTimer;
  /**
   * \brief Saved gif frames
   */
  QGifImage *gifFrames = nullptr;
};

}  // namespace s21

#endif  // SCREENCASTER_H_
