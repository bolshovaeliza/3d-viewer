#include "SettingsManager.h"

void s21::SettingsManager::notify() {
  for (const auto& x : observers) {
    x.get().update(m_referenceSettings);
  }
}

void s21::SettingsManager::attach(Settings& observer) {
  observers.push_front(observer);
  observer.update(m_referenceSettings);
}

void s21::SettingsManager::detach(s21::Settings& observer) {
  observers.remove_if(
      [&observer](const s21::SettingsManager::SettingsRef& obj) {
        return &obj.get() == &observer;
      });
}

s21::Settings& s21::SettingsManager::referenceSettings() {
  return m_referenceSettings;
}

void s21::SettingsManager::setReferenceSettings(s21::Settings& observer) {
  m_referenceSettings = observer;
  connect(&m_referenceSettings, &s21::Settings::settingsChanged, this,
          &s21::SettingsManager::notify);
  notify();
}

void s21::SettingsManager::setReferenceSettings(s21::Settings&& observer) {
  m_referenceSettings = std::move(observer);
  connect(&m_referenceSettings, &s21::Settings::settingsChanged, this,
          &s21::SettingsManager::notify);
  notify();
}
