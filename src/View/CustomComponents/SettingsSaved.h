#ifndef SETTINGSSAVED_H_
#define SETTINGSSAVED_H_

#include <QButtonGroup>
#include <QColor>
#include <QColorDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QRadioButton>
#include <QSettings>
#include <QVBoxLayout>
#include <QWidget>

#include "FloatInputWidget.h"
#include "Settings.h"

namespace s21 {
/**
 * \class SettingsSaved
 * \brief Class responsible for saving settings between runs.
 * \author Ammoshri
 * \details This is a child of Settings and can be managed by the
 * SettingsManager object.
 */
class SettingsSaved : public s21::Settings {
 public:
  /**
   * \brief Update settings.
   * \param settings - new settings.
   */
  virtual void update(Settings& settings);

  /**
   * \brief Load settings when the program starts.
   */
  void loadSettings();

  /**
   * \brief Get saved settings.
   * \return Settings - settings object.
   */
  Settings getSettings();

 private:
  /**
   * \brief QSettings objects that allows to access settings saved in the
   * system.
   */
  QSettings* viewerSettings = nullptr;

  /**
   * \brief Save settings inside the system.
   */
  void saveSettings();
};

}  // namespace s21

#endif  // SETTINGSSAVED_H_
