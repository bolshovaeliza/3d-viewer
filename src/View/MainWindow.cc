#include "MainWindow.h"

s21::MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent) {  // Initialize the main window
  resize(1920, 1080);

  QToolBar* sideMenu = new QToolBar();
  addToolBar(Qt::RightToolBarArea, sideMenu);
  sideMenu->setAllowedAreas(Qt::RightToolBarArea | Qt::LeftToolBarArea);
  sideMenu->layout()->setContentsMargins(5, 5, 5, 5);

  s21::ModelPositionChangeMenu* modelPosition =
      new s21::ModelPositionChangeMenu();
  sideMenu->addWidget(modelPosition);
  sideMenu->addSeparator();

  s21::Screencaster* screencaster = new s21::Screencaster();
  sideMenu->addWidget(screencaster);
  sideMenu->addSeparator();

  s21::SettingsWidget* settingsWidget = new s21::SettingsWidget();
  sideMenu->addWidget(settingsWidget);

  QToolBar* topMenu = new QToolBar();
  addToolBar(Qt::TopToolBarArea, topMenu);
  topMenu->setAllowedAreas(Qt::TopToolBarArea | Qt::BottomToolBarArea);
  topMenu->layout()->setContentsMargins(5, 5, 5, 5);

  s21::FileSelectWidget* fileSelect = new s21::FileSelectWidget();
  topMenu->addWidget(fileSelect);

  s21::OpenGLWidget* view = new s21::OpenGLWidget();
  setCentralWidget(view);
  view->setLoadedModel(controller.defaultModel());
  settingsWidget->settingsManager().attach(view->settings());

  connect(&view->settings(), &s21::Settings::settingsChanged,
          [view]() { view->update(); });

  view->update();

  connect(&controller, &s21::Controller::exceptionCaught,
          [this, fileSelect](QString what) {
            QMessageBox msgBox;
            msgBox.setText("ERROR: " + what);
            msgBox.exec();
          });
  connect(fileSelect, &s21::FileSelectWidget::filePathChanged,
          [this, view, modelPosition, fileSelect](QString filepath) {
            view->setLoadedModel(controller.loadModelFromFile(filepath));
            modelPosition->emit reset();
            fileSelect->setEdgeNumber(view->loadedModel().facetsSize());
            fileSelect->setVertexNumber(view->loadedModel().verticesAmount());
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::shiftXChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelShiftX(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::shiftYChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelShiftY(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::shiftZChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelShiftZ(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::rotationXChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelRotationX(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::rotationYChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelRotationY(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::rotationZChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelRotationZ(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::scaleChanged,
          [this, view, screencaster](double value) {
            view->setLoadedModel(controller.modelScale(value));
          });
  connect(modelPosition, &s21::ModelPositionChangeMenu::reset,
          [this, view, screencaster]() {
            view->setLoadedModel(controller.reset());
          });

  connect(
      screencaster, &s21::Screencaster::makeScreencastJpeg,
      [this, view, screencaster]() { screencaster->saveJpeg(view->grab()); });
  connect(
      screencaster, &s21::Screencaster::makeScreencastBmp,
      [this, view, screencaster]() { screencaster->saveBmp(view->grab()); });
  connect(screencaster, &s21::Screencaster::makeScreencastGif,
          [this, view, screencaster]() {
            screencaster->addGifFrame(view->grabFramebuffer());
          });
}

s21::MainWindow::~MainWindow() {}
