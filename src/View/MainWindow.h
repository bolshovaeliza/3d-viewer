#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QDebug>
#include <QMainWindow>
#include <QMessageBox>
#include <QToolBar>
#include <QVBoxLayout>

#include "../Controller/Controller.h"
#include "CustomComponents/CustomComponents.h"

namespace s21 {

/**
 * \class MainWindow
 * \brief This object is the main window of the application.
 * \author Ammoshri
 *
 */
class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  /**
   * \brief Default constructor.
   * \param parent - parent widget.
   */
  MainWindow(QWidget *parent = nullptr);
  /**
   * \brief Destructor.
   */
  ~MainWindow();

 private:
  /**
   * \brief Controller object.
   * \details This object connects to the Model object responsible for the logic
   * of the application.
   */
  s21::Controller controller;
};
}  // namespace s21

#endif  // MAINWINDOW_HP
