var hierarchy =
[
    [ "s21::Model", "classs21_1_1Model.html", null ],
    [ "s21::ModelData", "classs21_1_1ModelData.html", null ],
    [ "QMainWindow", null, [
      [ "s21::MainWindow", "classs21_1_1MainWindow.html", null ]
    ] ],
    [ "QObject", null, [
      [ "s21::Controller", "classs21_1_1Controller.html", null ],
      [ "s21::Settings", "classs21_1_1Settings.html", [
        [ "s21::SettingsSaved", "classs21_1_1SettingsSaved.html", null ]
      ] ],
      [ "s21::SettingsManager", "classs21_1_1SettingsManager.html", null ]
    ] ],
    [ "QOpenGLFunctions", null, [
      [ "s21::OpenGLWidget", "classs21_1_1OpenGLWidget.html", null ]
    ] ],
    [ "QOpenGLWidget", null, [
      [ "s21::OpenGLWidget", "classs21_1_1OpenGLWidget.html", null ]
    ] ],
    [ "QWidget", null, [
      [ "s21::FileSelectWidget", "classs21_1_1FileSelectWidget.html", null ],
      [ "s21::FloatInputWidget", "classs21_1_1FloatInputWidget.html", null ],
      [ "s21::ModelPositionChangeMenu", "classs21_1_1ModelPositionChangeMenu.html", null ],
      [ "s21::Screencaster", "classs21_1_1Screencaster.html", null ],
      [ "s21::SettingsWidget", "classs21_1_1SettingsWidget.html", null ]
    ] ]
];