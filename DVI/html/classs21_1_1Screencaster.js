var classs21_1_1Screencaster =
[
    [ "Screencaster", "classs21_1_1Screencaster.html#af3b1a102e0b11e26eb142d9f6378a1a0", null ],
    [ "~Screencaster", "classs21_1_1Screencaster.html#a8ab6b08278fda8ba9def09ea5aa4c26e", null ],
    [ "addGifFrame", "classs21_1_1Screencaster.html#a4b5660762c06c99bb30ea61a63d704be", null ],
    [ "makeScreencastBmp", "classs21_1_1Screencaster.html#aaf247237e24a845c34a1f674fee922d1", null ],
    [ "makeScreencastGif", "classs21_1_1Screencaster.html#ae20732bd41f463c61c9d6bb01796e677", null ],
    [ "makeScreencastJpeg", "classs21_1_1Screencaster.html#a70474d478d5a61dadee4cdc79d9cf3a0", null ],
    [ "now", "classs21_1_1Screencaster.html#ac273a1c0359a8c909dcc810323f8a3c0", null ],
    [ "saveBmp", "classs21_1_1Screencaster.html#a60ae9abe88a25d29a13e74a71aef4492", null ],
    [ "saveGif", "classs21_1_1Screencaster.html#a0ff8de26898782ed2d7237b4a92b23a6", null ],
    [ "saveJpeg", "classs21_1_1Screencaster.html#af5d9216118bcbfa7031a33f9194e9d71", null ],
    [ "startGifScreencasting", "classs21_1_1Screencaster.html#a2f87594b1bec12ca072bf96514243566", null ],
    [ "startLongTimer", "classs21_1_1Screencaster.html#afd2dab8bb7943b2b919dc3f0eec4e21d", null ],
    [ "startShortTimer", "classs21_1_1Screencaster.html#a1ce2bab6639ef97b9b3ab8a2c0a59e51", null ],
    [ "gifFrames", "classs21_1_1Screencaster.html#ab9d407f07e95b8ad798ac4821d8c71ce", null ],
    [ "longTimer", "classs21_1_1Screencaster.html#a8cbf32ecf6acc1aabb3af62f58815fd5", null ],
    [ "shortTimer", "classs21_1_1Screencaster.html#a7f132e86ece7b7705f8ef1b66a54e9df", null ]
];