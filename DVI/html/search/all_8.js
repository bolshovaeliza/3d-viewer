var searchData=
[
  ['incrementvalue_30',['incrementValue',['../classs21_1_1FloatInputWidget.html#af657a0a78852a63b9c12ab663e1d5c4c',1,'s21::FloatInputWidget']]],
  ['incrementvalueextra_31',['incrementValueExtra',['../classs21_1_1FloatInputWidget.html#a6a998195ba9fa2f0cd4d2a37aa4feaf2',1,'s21::FloatInputWidget']]],
  ['initializegl_32',['initializeGL',['../classs21_1_1OpenGLWidget.html#a4f4ea7a0dccb9bb0fb01250810dee2b4',1,'s21::OpenGLWidget']]],
  ['islinesolid_33',['isLineSolid',['../classs21_1_1Settings.html#a867fef9f57f45425cec09133207d842f',1,'s21::Settings']]],
  ['isprojectionparallel_34',['isProjectionParallel',['../classs21_1_1Settings.html#a88a92fa9513206f5514257241da5c122',1,'s21::Settings']]],
  ['isverticescircle_35',['isVerticesCircle',['../classs21_1_1Settings.html#a3583847c79e8c4425ffb85c3f4826505',1,'s21::Settings']]],
  ['isverticesvisible_36',['isVerticesVisible',['../classs21_1_1Settings.html#aa88ebe400f8e9c74a236ccb2de92a5dd',1,'s21::Settings']]]
];
