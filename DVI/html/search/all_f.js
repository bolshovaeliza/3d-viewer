var searchData=
[
  ['savebmp_90',['saveBmp',['../classs21_1_1Screencaster.html#a60ae9abe88a25d29a13e74a71aef4492',1,'s21::Screencaster']]],
  ['savegif_91',['saveGif',['../classs21_1_1Screencaster.html#a0ff8de26898782ed2d7237b4a92b23a6',1,'s21::Screencaster']]],
  ['savejpeg_92',['saveJpeg',['../classs21_1_1Screencaster.html#af5d9216118bcbfa7031a33f9194e9d71',1,'s21::Screencaster']]],
  ['savesettings_93',['saveSettings',['../classs21_1_1SettingsSaved.html#a35405ebf7d0d095b483f6b7e01fd5fd2',1,'s21::SettingsSaved']]],
  ['scalechanged_94',['scaleChanged',['../classs21_1_1ModelPositionChangeMenu.html#a7f3354cb4440d3aff5bd534224ca05af',1,'s21::ModelPositionChangeMenu']]],
  ['screencaster_95',['Screencaster',['../classs21_1_1Screencaster.html',1,'s21::Screencaster'],['../classs21_1_1Screencaster.html#af3b1a102e0b11e26eb142d9f6378a1a0',1,'s21::Screencaster::Screencaster()']]],
  ['setbackgroundcolor_96',['setBackgroundColor',['../classs21_1_1Settings.html#a137ea4942315ad9284ef066105aeaec6',1,'s21::Settings']]],
  ['setdescription_97',['setDescription',['../classs21_1_1FloatInputWidget.html#aa329930684bf85ed933820b8ca1d32a4',1,'s21::FloatInputWidget']]],
  ['setedgenumber_98',['setEdgeNumber',['../classs21_1_1FileSelectWidget.html#a0f9d1ef4b1f3c6472bc1fdb47e217845',1,'s21::FileSelectWidget']]],
  ['setfilepath_99',['setFilePath',['../classs21_1_1FileSelectWidget.html#a6122d7ffa142c660000e617a44f56fa3',1,'s21::FileSelectWidget']]],
  ['setislinesolid_100',['setIsLineSolid',['../classs21_1_1Settings.html#a023fbdb8449b8dbc0a9c1b7b2f09111b',1,'s21::Settings']]],
  ['setisprojectionparallel_101',['setIsProjectionParallel',['../classs21_1_1OpenGLWidget.html#aca03d5657ce726c69ff16f6786f72db7',1,'s21::OpenGLWidget::setIsProjectionParallel()'],['../classs21_1_1Settings.html#a006520666405b9f5d3b48c2daa971e35',1,'s21::Settings::setIsProjectionParallel(bool value)']]],
  ['setisverticescircle_102',['setIsVerticesCircle',['../classs21_1_1Settings.html#adc6495930c658a4bd11c6493146eca98',1,'s21::Settings']]],
  ['setisverticesvisible_103',['setIsVerticesVisible',['../classs21_1_1Settings.html#aa890f5434673c8979b90bc6bc34b44d2',1,'s21::Settings']]],
  ['setlinecolor_104',['setLineColor',['../classs21_1_1Settings.html#ae6dd4d66170e53d2c9e8bf6ec8a9213a',1,'s21::Settings']]],
  ['setlinewidth_105',['setLineWidth',['../classs21_1_1Settings.html#a3cc52f2ac3dfa6fdc8ce0edb5cf0deb4',1,'s21::Settings']]],
  ['setloadedmodel_106',['setLoadedModel',['../classs21_1_1OpenGLWidget.html#a72919f4bb85ae6858d1b0c9ee2b746d6',1,'s21::OpenGLWidget::setLoadedModel(const s21::ModelData &amp;newModel)'],['../classs21_1_1OpenGLWidget.html#aa8e09a0dc9906e04e98a569b67d77d3b',1,'s21::OpenGLWidget::setLoadedModel(s21::ModelData &amp;&amp;newModel)']]],
  ['setreferencesettings_107',['setReferenceSettings',['../classs21_1_1SettingsManager.html#a561e51f20d7b21c77c14e58dcaa6aa42',1,'s21::SettingsManager::setReferenceSettings(Settings &amp;observer)'],['../classs21_1_1SettingsManager.html#a5ba8b562114762aff072909a13ea5520',1,'s21::SettingsManager::setReferenceSettings(Settings &amp;&amp;observer)']]],
  ['setrotationx_108',['setRotationX',['../classs21_1_1Model.html#a4d0ddb7e247767af29bb09b7ada78416',1,'s21::Model']]],
  ['setrotationy_109',['setRotationY',['../classs21_1_1Model.html#ab931552e294f0c3ca32bb0e1860241bb',1,'s21::Model']]],
  ['setrotationz_110',['setRotationZ',['../classs21_1_1Model.html#ae03efab60d477892b7ccfe9c6c95a48b',1,'s21::Model']]],
  ['setscale_111',['setScale',['../classs21_1_1Model.html#ae0c064b19da7f870edfc8d1909f7d3e0',1,'s21::Model']]],
  ['setshiftx_112',['setShiftX',['../classs21_1_1Model.html#ab8f7af8a6e5328e8212657198c679c9b',1,'s21::Model']]],
  ['setshifty_113',['setShiftY',['../classs21_1_1Model.html#a753ddb789be5039c295440c77a538ab9',1,'s21::Model']]],
  ['setshiftz_114',['setShiftZ',['../classs21_1_1Model.html#a56b5a92d0114da3e0e77d3688ac1eef1',1,'s21::Model']]],
  ['settings_115',['Settings',['../classs21_1_1Settings.html',1,'s21']]],
  ['settings_116',['settings',['../classs21_1_1OpenGLWidget.html#adfc7ba6b2c32df23123eebe7cd35c8d3',1,'s21::OpenGLWidget']]],
  ['settings_117',['Settings',['../classs21_1_1Settings.html#ac97212e2f47321a1f509cc78f4d3582c',1,'s21::Settings::Settings()'],['../classs21_1_1Settings.html#a40c8b212b2236f12282cea5929552edf',1,'s21::Settings::Settings(const Settings &amp;other)'],['../classs21_1_1Settings.html#a81709ced4c204d3036f26441b3709b3d',1,'s21::Settings::Settings(Settings &amp;&amp;other)']]],
  ['settingschanged_118',['settingsChanged',['../classs21_1_1Settings.html#a9384f06c525d6e29ed6e5e372b57b52a',1,'s21::Settings']]],
  ['settingsmanager_119',['SettingsManager',['../classs21_1_1SettingsManager.html',1,'s21']]],
  ['settingsmanager_120',['settingsManager',['../classs21_1_1SettingsWidget.html#add6c257221d6874159d9dd2e03637f0e',1,'s21::SettingsWidget']]],
  ['settingsref_121',['SettingsRef',['../classs21_1_1SettingsManager.html#a9acafa931db5117eb4167e388616d12f',1,'s21::SettingsManager']]],
  ['settingssaved_122',['SettingsSaved',['../classs21_1_1SettingsSaved.html',1,'s21']]],
  ['settingswidget_123',['SettingsWidget',['../classs21_1_1SettingsWidget.html',1,'s21::SettingsWidget'],['../classs21_1_1SettingsWidget.html#a3e4a6d01d7c4402cafa4a0986546f72c',1,'s21::SettingsWidget::SettingsWidget()']]],
  ['setvalue_124',['setValue',['../classs21_1_1FloatInputWidget.html#ac8e932edc0336ced1df9f3f6893b36ae',1,'s21::FloatInputWidget']]],
  ['setvertexnumber_125',['setVertexNumber',['../classs21_1_1FileSelectWidget.html#a20634e3727374604f22c974df1792761',1,'s21::FileSelectWidget']]],
  ['setverticescolor_126',['setVerticesColor',['../classs21_1_1Settings.html#a40416c5eb2619d04e297e4f6ee4e9440',1,'s21::Settings']]],
  ['setverticessize_127',['setVerticesSize',['../classs21_1_1Settings.html#a7713b7e11f04fbdab8e2353ff3dfac06',1,'s21::Settings']]],
  ['shiftxchanged_128',['shiftXChanged',['../classs21_1_1ModelPositionChangeMenu.html#a13adb93a5c1fd0f096167be1a7d2b03d',1,'s21::ModelPositionChangeMenu']]],
  ['shiftychanged_129',['shiftYChanged',['../classs21_1_1ModelPositionChangeMenu.html#a0a9ca4b0d44320c1b0fdbe5def7c1814',1,'s21::ModelPositionChangeMenu']]],
  ['shiftzchanged_130',['shiftZChanged',['../classs21_1_1ModelPositionChangeMenu.html#a5b805cdc9d6cd16f38878f79b6eae60e',1,'s21::ModelPositionChangeMenu']]],
  ['shorttimer_131',['shortTimer',['../classs21_1_1Screencaster.html#a7f132e86ece7b7705f8ef1b66a54e9df',1,'s21::Screencaster']]],
  ['startgifscreencasting_132',['startGifScreencasting',['../classs21_1_1Screencaster.html#a2f87594b1bec12ca072bf96514243566',1,'s21::Screencaster']]],
  ['startlongtimer_133',['startLongTimer',['../classs21_1_1Screencaster.html#afd2dab8bb7943b2b919dc3f0eec4e21d',1,'s21::Screencaster']]],
  ['startshorttimer_134',['startShortTimer',['../classs21_1_1Screencaster.html#a1ce2bab6639ef97b9b3ab8a2c0a59e51',1,'s21::Screencaster']]]
];
