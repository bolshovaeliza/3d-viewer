var classs21_1_1SettingsManager =
[
    [ "SettingsRef", "classs21_1_1SettingsManager.html#a9acafa931db5117eb4167e388616d12f", null ],
    [ "attach", "classs21_1_1SettingsManager.html#a2dc9eb927d502578141d9748be60e580", null ],
    [ "detach", "classs21_1_1SettingsManager.html#a641cffe33fffecf3b76f249a9ce26a45", null ],
    [ "notify", "classs21_1_1SettingsManager.html#ac5c4d300d41e11dae84eee27dc5c4e33", null ],
    [ "referenceSettings", "classs21_1_1SettingsManager.html#adbca9d02d461f180f4695d7574340b5c", null ],
    [ "setReferenceSettings", "classs21_1_1SettingsManager.html#a5ba8b562114762aff072909a13ea5520", null ],
    [ "setReferenceSettings", "classs21_1_1SettingsManager.html#a561e51f20d7b21c77c14e58dcaa6aa42", null ],
    [ "m_referenceSettings", "classs21_1_1SettingsManager.html#ace1e9b1c35a18f68c6ce91900f7d3dea", null ],
    [ "observers", "classs21_1_1SettingsManager.html#a71a45174085cf8cb9a90b5357e479c04", null ]
];