var annotated_dup =
[
    [ "s21", null, [
      [ "Controller", "classs21_1_1Controller.html", "classs21_1_1Controller" ],
      [ "FileSelectWidget", "classs21_1_1FileSelectWidget.html", "classs21_1_1FileSelectWidget" ],
      [ "FloatInputWidget", "classs21_1_1FloatInputWidget.html", "classs21_1_1FloatInputWidget" ],
      [ "ModelPositionChangeMenu", "classs21_1_1ModelPositionChangeMenu.html", "classs21_1_1ModelPositionChangeMenu" ],
      [ "OpenGLWidget", "classs21_1_1OpenGLWidget.html", "classs21_1_1OpenGLWidget" ],
      [ "Screencaster", "classs21_1_1Screencaster.html", "classs21_1_1Screencaster" ],
      [ "Settings", "classs21_1_1Settings.html", "classs21_1_1Settings" ],
      [ "SettingsManager", "classs21_1_1SettingsManager.html", "classs21_1_1SettingsManager" ],
      [ "SettingsSaved", "classs21_1_1SettingsSaved.html", "classs21_1_1SettingsSaved" ],
      [ "SettingsWidget", "classs21_1_1SettingsWidget.html", "classs21_1_1SettingsWidget" ],
      [ "MainWindow", "classs21_1_1MainWindow.html", "classs21_1_1MainWindow" ],
      [ "Model", "classs21_1_1Model.html", "classs21_1_1Model" ],
      [ "ModelData", "classs21_1_1ModelData.html", "classs21_1_1ModelData" ]
    ] ]
];