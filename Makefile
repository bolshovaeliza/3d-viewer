PROJECT_NAME=3D_Viewer
PROJECT_DIR=$(CURDIR)
BUILD_DIR=${PROJECT_DIR}/build
SOURCE_DIR=${PROJECT_DIR}/src
MODEL_DIR=${PROJECT_DIR}/src/Model
TEST_DIR=${MODEL_DIR}/tests


# //----------------------------------------  ALL   ---------------------------------------//

all:
	@echo '  "._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."  '
	@echo "|                                                                              |"
	@echo "$(shell printf '|     PROJECT %s ' '${PROJECT_NAME}' | awk '{printf "%-79s", $$0}')|"
	@echo "|                                                                              |"
	@echo '  "._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."._."  '
	@echo
	@make clean
	@echo
	@make uninstall
	@echo
	@make install 
	@echo
	@make run 
	@echo

# //--------------------------------------   INSTALL   --------------------------------------//

PROJECT_BUILD_DIR=${BUILD_DIR}/${PROJECT_NAME}
INSTALL_DIR=${PROJECT_DIR}/install

compile: 
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     BUILDING THE VIEWER                                                      |"
	@echo "+------------------------------------------------------------------------------+"
	@mkdir -p ${PROJECT_BUILD_DIR}
	@cmake -S ${SOURCE_DIR} -B ${PROJECT_BUILD_DIR}
	@cmake --build ${PROJECT_BUILD_DIR}

install: compile
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     INSTALLING THE VIEWER                                                    |"
	@echo "+------------------------------------------------------------------------------+"
	@echo "INSTALLING TO" ${INSTALL_DIR}
	@mkdir -p ${INSTALL_DIR}
	@cp ${PROJECT_BUILD_DIR}/${PROJECT_NAME} ${INSTALL_DIR}/${PROJECT_NAME}

run:
	${INSTALL_DIR}/${PROJECT_NAME}

uninstall:
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     UNINSTALLING THE VIEWER                                                  |"
	@echo "+------------------------------------------------------------------------------+"
	@rm -rf ${INSTALL_DIR}
	@echo "SUCCESS"

# //----------------------------------------   DVI   ----------------------------------------//

DVI_SOURCE=${SOURCE_DIR}/Doxyfile
DVI_OUTPUT_FOLDER=${PROJECT_DIR}/DVI

dvi:
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     GENERATING DVI                                                           |"
	@echo "+------------------------------------------------------------------------------+"
	doxygen ${DVI_SOURCE}
	open ${DVI_OUTPUT_FOLDER}/html/index.html

# //---------------------------------------   DIST   ----------------------------------------//

DIST_NAME=${PROJECT_DIR}/${PROJECT_NAME}.tar.gz

dist: 
	@rm -rf ${DIST_NAME}
	git archive --format=tar.gz -o ${DIST_NAME} --prefix=my-repo/ develop

# //---------------------------------------   TEST   ----------------------------------------//

PROJECT_NAME_TESTS=${PROJECT_NAME}_Tests
TEST_BUILD_DIR=${BUILD_DIR}/${PROJECT_NAME_TESTS}
TEST_EXEC=${TEST_BUILD_DIR}/s21_test

MODEL_SOURCES=$(wildcard ${MODEL_DIR}/*.cc ${MODEL_DIR}/*.h)
TEST_SOURCES=$(wildcard ${TEST_DIR}/Tests*.cc)

CC=g++
CFLAGS= -Wall -Werror -Wextra -std=c++17
GTEST=-lgtest #-lgtest_main

ifeq ($(shell uname -s), Linux)
	LEAKS=valgrind --tool=memcheck --leak-check=yes
else 
	LEAKS=leaks -atExit -- 
endif

compile_tests: 
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     COMPILING TESTS                                                          |"
	@echo "+------------------------------------------------------------------------------+"
	mkdir -p ${TEST_BUILD_DIR}
	cp ${TEST_DIR}/test_resources/* ${TEST_BUILD_DIR}
	$(CC) $(CFLAGS) ${MODEL_SOURCES} ${TEST_SOURCES} -o ${TEST_EXEC} ${GTEST}

tests: compile_tests
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     RUNNING TESTS                                                            |"
	@echo "+------------------------------------------------------------------------------+"
	cd ${TEST_BUILD_DIR} && ${TEST_EXEC}

tests_leaks: compile_tests
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     CHECKING LEAKS IN TESTS                                                  |"
	@echo "+------------------------------------------------------------------------------+"
	cd ${TEST_BUILD_DIR} && ${LEAKS} ${TEST_EXEC} ${TEST_EXEC}


coverage: clean
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     GENERATING COVERAGE REPORT                                               |"
	@echo "+------------------------------------------------------------------------------+"
	$(CC) $(CFLAGS) --coverage ${TEST_DIR}/Tests.cc ${TEST_DIR}/TestsModel.cc  ${TEST_DIR}/TestsModelData.cc ${MODEL_DIR}/Model.cc ${MODEL_DIR}/ModelData.cc -o ${TEST_DIR}/test_resources/s21_test $(GTEST)
	cd ${TEST_DIR}/test_resources/ && ./s21_test
	mkdir -p ${SOURCE_DIR}/report
	gcovr --html --html-details report/index.html
	rm -rf  ${TEST_DIR}/test_resources/s21_test*
	open ${SOURCE_DIR}/report/index.html



# //---------------------------------------   CLANG   ---------------------------------------//

style_check:
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     CHECKING STYLE USING CLANG                                               |"
	@echo "+------------------------------------------------------------------------------+"
	find ${SOURCE_DIR} -type f -name "*.h" -o -name "*.hpp" -o -name "*.cc" | xargs clang-format -style=Google -n
	@echo "Successfully checked style!"

style_fix:
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     FIXING STYLE USING CLANG                                                 |"
	@echo "+------------------------------------------------------------------------------+"
	find ${SOURCE_DIR} -type f -name "*.h" -o -name "*.hpp" -o -name "*.cc" | xargs clang-format -style=Google -i
	@echo "Successfully fixed style!"


# //---------------------------------------   CLEAN   ---------------------------------------//

clean:
	@echo "+------------------------------------------------------------------------------+"
	@echo "|     CLEANING                                                                 |"
	@echo "+------------------------------------------------------------------------------+"
	rm -rf ${BUILD_DIR}
	rm -rf ${DIST_NAME}
	rm -rf ${DVI_OUTPUT_FOLDER}
	rm -rf ${SOURCE_DIR}/report
	rm -rf  ${TEST_DIR}/test_resources/s21_test*

