# 3D Viewer

3D Viewer is a program for viewing wireframe 3D models. The program is developed in C++ language of C++17 standard and is written in accordance with Google stype.


## Code

The program is built with Makefile which contains standard set of targets for GNU-programs: all, install, uninstall, clean, dvi, dist, tests. Program is built into `build` directory and installed into `install` directory.

The program is developed according to the principles of object-oriented programming. Modules related to model loading and affine transformations are covered with unit-tests.

The program is implemented using the MVC pattern. Classes are implemented within the `s21` namespace.


## GUI

GUI implementation is written using Qt.
The program provides the ability to:
  - Load a wireframe model from an obj file (vertices and surfaces list support only);
  - Translate the model by a given distance in relation to the X, Y, Z axes;
  - Rotate the model by a given angle in relation to its X, Y, Z axes;
  - Scale the model by a given value;
The graphical user interface contains contain:
  - A button to select the model file and a field to output its name;
  - A visualisation area for the wireframe model;
  - Button/buttons and input fields for translating the model;
  - Button/buttons and input fields for rotating the model;
  - Button/buttons and input fields for scaling the model;
  - Information about the uploaded model - file name, number of vertices and edges;


### View customization

- The program allows customizing the type of projection (parallel and central);
- The program allows setting up the type (solid, dashed), color and thickness of the edges, display method (none, circle, square), color and size of the vertices;
- The program allows choosing the background color;
- Settings are saved between program restarts.

### Render saving

- The program allows saving the captured (rendered) images as bmp and jpeg files;
- The program allows recording small screencasts - the current custom affine transformation of the loaded object into gif-animation (640x480, 10fps, 5s) by a special button.
